use virtual_teacher;

create table topics
(
    topic_id   bigint auto_increment
        primary key,
    topic      varchar(255) null,
    is_deleted tinyint(1)   null
);

create table users
(
    user_id    bigint auto_increment
        primary key,
    email      varchar(255)                                                      null,
    username   varchar(50)                                                       not null,
    password   varchar(255)                                                      null,
    first_name varchar(50)                                                       null,
    last_name  varchar(50)                                                       null,
    picture    varchar(255)                                                      null,
    enabled    tinyint(1)                                                        null,
    role       enum ('ADMIN', 'STUDENT', 'TEACHER', 'WAITING') default 'STUDENT' null,
    constraint users_username_uindex
        unique (username)
);

create table authorities
(
    username  varchar(50)  not null,
    authority varchar(255) not null,
    primary key (authority, username),
    constraint authorities_users_username_fk
        foreign key (username) references users (username)
);

create table courses
(
    course_id   bigint auto_increment
        primary key,
    title       varchar(255)         null,
    description text                 null,
    topic_id    bigint               null,
    user_id     bigint               null,
    avg_rating  double               null,
    is_deleted  tinyint(1)           null,
    picture     varchar(255)         null,
    is_finished tinyint(1) default 0 null,
    constraint courses_topics_topic_id_fk
        foreign key (topic_id) references topics (topic_id),
    constraint courses_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table lectures
(
    lecture_id  bigint auto_increment
        primary key,
    title       varchar(255) null,
    description varchar(255) null,
    is_deleted  tinyint(1)   null,
    course_id   bigint       not null,
    video       varchar(255) null,
    assignment  varchar(255) null,
    constraint lectures_courses_course_id_fk
        foreign key (course_id) references courses (course_id)
);

create table finished_lectures_users
(
    lecture_id bigint not null,
    user_id    bigint not null,
    primary key (lecture_id, user_id),
    constraint finished_lectures_users_lectures_lecture_id_fk
        foreign key (lecture_id) references lectures (lecture_id),
    constraint finished_lectures_users_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table homeworks
(
    homework_id bigint auto_increment
        primary key,
    homework    varchar(255) null,
    grade       int          null,
    user_id     bigint       null,
    lecture_id  bigint       not null,
    course_id   bigint       null,
    constraint FK_homeworks_users
        foreign key (user_id) references users (user_id),
    constraint homeworks_courses_course_id_fk
        foreign key (course_id) references courses (course_id),
    constraint homeworks_lectures_lecture_id_fk
        foreign key (lecture_id) references lectures (lecture_id)
);

create table ratings
(
    rating_id bigint auto_increment
        primary key,
    rating    int    null,
    course_id bigint null,
    user_id   bigint null,
    constraint FK_ratings_courses
        foreign key (course_id) references courses (course_id),
    constraint FK_ratings_users
        foreign key (user_id) references users (user_id)
);

create table students_courses
(
    user_id   bigint not null,
    course_id bigint not null,
    constraint FK_students_courses_courses
        foreign key (course_id) references courses (course_id),
    constraint FK_students_courses_users
        foreign key (user_id) references users (user_id)
);

