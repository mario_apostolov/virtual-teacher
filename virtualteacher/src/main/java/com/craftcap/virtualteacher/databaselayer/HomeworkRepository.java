package com.craftcap.virtualteacher.databaselayer;

import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface HomeworkRepository extends JpaRepository<Homework, Long> {
    List<Homework> findAllByCourseIdAndGrade(long id, int grade);
    Homework findHomeworkById(long id);
    boolean existsByLectureAndStudent(Lecture lecture, User user);

    @Query("select avg (grade) from Homework where user_id = :userId and course_id = :courseId")
    Integer getAvgGradeByCourseByStudent(@Param("userId") long userId, @Param("courseId") long courseId);

    @Query("select count (id) from Homework where user_id = :userId and course_id = :courseId")
    int getNumberOfHomeworkPerStudentPerCourse(@Param("userId") long userId, @Param("courseId") long courseId);

    Homework findHomeworkByLectureAndStudent(Lecture lecture, User student);
}
