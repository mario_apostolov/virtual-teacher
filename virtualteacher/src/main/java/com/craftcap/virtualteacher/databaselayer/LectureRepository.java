package com.craftcap.virtualteacher.databaselayer;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LectureRepository extends JpaRepository<Lecture, Long> {
    List<Lecture> findAllByCourseId(long id);
    Lecture findLectureById(long id);

    @Query("select count (id) from Lecture where course_id = :courseId")
    int getNumberOfLecturesPerCourse(@Param("courseId") long courseId);

    @Query("select l from Lecture l join l.students s on s.username = :username where l.course = :course")
    List<Lecture> getFinishedLecturesByCourseByUser(@Param("course") Course course, @Param("username") String username);

    @Query("select l from Lecture l join l.homeworkOfStudents h on h.lecture = l.id where h.student = :user and l.course = :course")
    List<Lecture> getFinished(@Param("user") User user, @Param("course") Course course);
}
