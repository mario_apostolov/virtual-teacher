package com.craftcap.virtualteacher.databaselayer;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SearchRepository extends JpaRepository<Course, Long> {

    @Query("select c from Course c where title like %:title% and is_deleted = false and is_finished = true")
    List<Course> searchCourseByTitle(@Param("title") String title);

    @Query("select c from Course c where description like %:description% and is_deleted = false and is_finished = true")
    List<Course> searchCourseByDescriptionAndFinishedIsTrueAndDeletedIsFalse(@Param("description") String description);

    List<Course> findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(User user);
}
