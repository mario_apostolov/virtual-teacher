package com.craftcap.virtualteacher.databaselayer;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Rating;
import com.craftcap.virtualteacher.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatingRepository extends JpaRepository<Rating, Long> {
    boolean existsByCourseAndUser(Course course, User user);
}
