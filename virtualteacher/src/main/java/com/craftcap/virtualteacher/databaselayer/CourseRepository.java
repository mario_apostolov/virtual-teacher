package com.craftcap.virtualteacher.databaselayer;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long> {
    Course findCourseByIdAndDeletedIsFalse(long courseId);
    Page<Course> getAllByFinishedIsTrueAndDeletedIsFalse(Pageable pageable);

    List<Course> findCoursesByFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc();

    Course findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(long id);

    Course findCourseByIdAndFinishedIsFalseAndDeletedIsFalse(long id);

    boolean existsCourseByTitleAndFinishedIsTrueAndDeletedIsFalse(String title);

    List<Course> getAllByTopicAndFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc(Topic topic);

    List<Course> findCoursesByCreatorIdAndFinishedIsTrueAndDeletedIsFalse(long id);

    boolean existsCourseByStudentsAndIdAndFinishedIsTrueAndDeletedIsFalse(User user, long id);

    Page<Course> findAllByTopicAndFinishedIsTrueAndDeletedIsFalse(Topic topic, Pageable pageable);

    List<Course> findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(User user);

    Page<Course> findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(User user, Pageable pageable);
    List<Course> findAllByCreatorAndDeletedIsFalse(User user);

    Page<Course> findAllByFinishedIsTrueAndDeletedIsFalseAndTitleOrDescriptionAndFinishedIsTrueAndDeletedIsFalseOrTopicAndFinishedIsTrueAndDeletedIsFalseOrCreatorAndFinishedIsTrueAndDeletedIsFalse(String title, String description, Topic topic, User creator, Pageable pageable);
}
