package com.craftcap.virtualteacher.databaselayer;

import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAllByEnabledIsTrueAndRole(Role role);
    User findUserByUsernameAndEnabledIsTrue(String username);
    Page<User> findAllByRole(Pageable pageable, Role role);

    @Query("select u from User u where first_name like %:name% or last_name like %:name%")
    List<User> searchUserByFirstNameOrLastName(@Param("name") String name);

    User findUserByRoleAndUsername(Role role, String username);
}
