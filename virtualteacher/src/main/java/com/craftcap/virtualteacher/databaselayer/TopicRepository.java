package com.craftcap.virtualteacher.databaselayer;

import com.craftcap.virtualteacher.models.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
    Topic getTopicByTopic(String topic);

    @Query("select t from Topic t where topic = :topic")
    List<Topic> findAllByTopic(@Param("topic") String topic);
}
