package com.craftcap.virtualteacher.constants;

public class AppConstants {

    public static final String EMAIL_ADDRESS = "agroacademy.company@gmail.com";
    public static final String EMAIL_PASSWORD = "agroAcademyPass";
    public static final int COURSES_PER_PAGE = 6;
    public static final int AVG_GRADE_TO_PASS = 51;
    public static final String RATE_COURSE_TWICE_ERROR_MESSAGE = "You cannot rate course twice, sorry";
    public static final String ACCESS_DENIED_ERROR_MESSAGE = "Access denied";
    public static final String ENROLL_TO_COURSE_TWICE_ERROR_MESSAGE = "You cannot enroll same course twice, sorry";
    public static final String RATE_COURSE_BEFORE_PASS_IT_ERROR_MESSAGE = "You have to pass course first to be able to rate it";
    public static final String COURSE_NOT_FOUND_ERROR_MESSAGE = "This course is not found";
    public static final String COURSE_ALREADY_DELETED_ERROR_MESSAGE = "This course is already deleted";
    public static final String COURSE_ALREADY_EXIST_ERROR_MESSAGE = "Course with this title already exist";
    public static final String HOMEWORK_NOT_FOUND_ERROR_MESSAGE = "This homework is not found";
    public static final String LECTURE_NOT_FOUND_ERROR_MESSAGE = "This lecture is not found";
    public static final String LECTURE_ALREADY_DELETED_ERROR_MESSAGE = "This lecture is already deleted";
    public static final String LECTURE_FROM_ANOTHER_COURSE_ERROR_MESSAGE = "Sorry, this lecture is not from this course";
    public static final String USER_NOT_FOUND_ERROR_MESSAGE = "No users to show.";
    public static final String USERNAME_LENGTH_ERROR_MESSAGE = "Username should be between 4 and 50 characters.";
    public static final String FIRST_NAME_LENGTH_ERROR_MESSAGE = "First name should be between 1 and 50 characters.";
    public static final String LAST_NAME_LENGTH_ERROR_MESSAGE = "Last name should be between 1 and 50 characters.";
    public static final String WRONG_FILE_FORMAT = "Wrong file format!";
    public static final String USER_ALREADY_DELETED_ERROR_MESSAGE = "This user is already deleted";
    public static final String VIDEO_DIRECTORY = "../../Files/Videos/";
    public static final String ASSIGNMENT_DIRECTORY = "../../Files/Assignments/";
    public static final String PHOTOS_DIRECTORY = "../../Files/Photos/";
    public static final String HOMEWORK_DIRECTORY = "../../Files/Homework/";
    public static final long HOMEWORK_MAX_SIZE = 2*1024*1024;
    public static final long ASSIGNMENT_MAX_SIZE = 2*1024*1024;
    public static final long VIDEO_MAX_SIZE = 250000000;
    public static final long IMAGE_MAX_SIZE = 2*1024*1024;

}
