package com.craftcap.virtualteacher.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class WrongFIleFormat extends RuntimeException {

    public WrongFIleFormat(String message) {
        super(message);
    }
}
