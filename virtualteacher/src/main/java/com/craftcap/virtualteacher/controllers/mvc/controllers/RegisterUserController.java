package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.dtos.NewUserDTO;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegisterUserController {
    private UserDetailsManager userDetailsManager;
    private UserService userService;

    @Autowired
    public RegisterUserController(UserDetailsManager userDetailsManager, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterUserPage(Model model) {
        model.addAttribute("student", true);
        model.addAttribute("user", new NewUserDTO());
        return "register";
    }

    @GetMapping("/register-teacher")
    public String showRegisterTeacherPage(Model model) {
        model.addAttribute("teacher", true);
        model.addAttribute("user", new NewUserDTO());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute NewUserDTO newUserDTO, BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
            newUserDTO.setPassword(null);
            model.addAttribute("user", newUserDTO);
            if(newUserDTO.getRole().equals(Role.TEACHER)){
                model.addAttribute("teacher", true);
                return "register";
            }
            model.addAttribute("student", true);
            return "register";
        }

        if (userDetailsManager.userExists(newUserDTO.getUsername())) {
            model.addAttribute("error", "User with the same name already exists!");
            if(newUserDTO.getRole().equals(Role.TEACHER)){
                model.addAttribute("teacher", true);
                return "register-teacher";
            }
            model.addAttribute("student", true);
            return "register";
        }
        userService.create(newUserDTO);
        return "login";
    }

}
