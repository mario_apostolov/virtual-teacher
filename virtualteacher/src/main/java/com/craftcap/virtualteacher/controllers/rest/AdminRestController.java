package com.craftcap.virtualteacher.controllers.rest;

import com.craftcap.virtualteacher.models.dtos.UserDTO;
import com.craftcap.virtualteacher.models.mappers.UserMapper;
import com.craftcap.virtualteacher.services.contracts.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/admin")
public class AdminRestController {
    private AdminService adminService;
    private UserMapper userMapper;

    @Autowired
    public AdminRestController(AdminService adminService, UserMapper userMapper) {
        this.adminService = adminService;
        this.userMapper = userMapper;
    }

    @GetMapping("/waiting")
    public List<UserDTO> getWaitingForApprovalTeachers(){
        return adminService.getAllEnabledWaiting()
                .stream()
                .map(user -> userMapper.fromUserToDTO(user))
                .collect(Collectors.toList());
    }
}
