package com.craftcap.virtualteacher.controllers.mvc.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;
import javax.validation.Valid;

import com.craftcap.virtualteacher.models.dtos.EmailDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EmailController {

    @PostMapping(value = "/send-email")
    public String sendEmail(@Valid @ModelAttribute EmailDTO emailDTO) throws IOException, MessagingException {
        sendmail(emailDTO);
        return "redirect:/";
    }

    private void sendmail(EmailDTO emailDTO) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("agroacademy.company@gmail.com", "agroAcademyPass");
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("agroacademy.company@gmail.com"));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("agroacademy.company@gmail.com"));
        msg.setSubject(String.format("Mail from %s", emailDTO.getName()));
        msg.setContent(String.format("Email address: %s\nBody: %s", emailDTO.getEmail(), emailDTO.getBody()), "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(String.format("Email address: %s\nBody: %s", emailDTO.getEmail(), emailDTO.getBody()), "text/html");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
//        MimeBodyPart attachPart = new MimeBodyPart();

//        attachPart.attachFile("/images/image19.png");
//        multipart.addBodyPart(attachPart);
        msg.setContent(multipart);
        Transport.send(msg);
    }

}