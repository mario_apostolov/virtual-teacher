package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.services.contracts.AdminService;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class TeachersPageController {
    private UserService userService;
    private AdminService adminService;
    private CourseService courseService;

    @Autowired
    public TeachersPageController(UserService userService, AdminService adminService, CourseService courseService) {
        this.userService = userService;
        this.adminService = adminService;
        this.courseService = courseService;
    }

    @GetMapping("/teachers")
    public String showTeachersPage(Model model, @RequestParam(required = false) Integer page){
        int students = adminService.getAllEnabledStudents().size();
        int teachers = adminService.getAllEnabledTeachers().size();
        int courses = courseService.getAllCourses().size();
        model.addAttribute("studentsSize", students);
        model.addAttribute("teachersSize", teachers);
        model.addAttribute("coursesSize", courses);
        model.addAttribute("teachers" , userService.getAllTeachers(page, Role.TEACHER));
        return "teachers";
    }
}
