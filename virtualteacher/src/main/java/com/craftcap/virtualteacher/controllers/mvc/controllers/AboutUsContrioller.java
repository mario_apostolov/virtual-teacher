package com.craftcap.virtualteacher.controllers.mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutUsContrioller {

    @GetMapping("/info")
    public String showAboutUsPage(){
        return "info";
    }
 }
