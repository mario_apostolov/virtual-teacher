package com.craftcap.virtualteacher.controllers.rest;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.CourseDTO;
import com.craftcap.virtualteacher.models.dtos.NewCourseDTO;
import com.craftcap.virtualteacher.models.mappers.CourseMapper;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/courses")
public class CourseRestController {
    private CourseService service;
    private CourseMapper courseMapper;
    private UserService userService;

    @Autowired
    public CourseRestController(CourseService service, CourseMapper courseMapper, UserService userService) {
        this.service = service;
        this.courseMapper = courseMapper;
        this.userService = userService;
    }
    
    @GetMapping
    public List<CourseDTO> getAllCourses(){
        return service.getAllCourses()
                .stream().map(course -> courseMapper.fromCourseToDTO(course)).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public CourseDTO getCourseById(@PathVariable long id){
        return courseMapper.fromCourseToDTO(service.getById(id));
    }

    @GetMapping("/topThreeByRating")
    public List<Course> getTopThreeCoursesByRating(){
        return service.getTopThreeCoursesByRating();
    }

    @PostMapping("/new-course")
    public CourseDTO createCourse(@Valid @RequestBody NewCourseDTO course, Principal principal){
        User user = userService.getByUsername(principal.getName());
        return courseMapper.fromCourseToDTO(service.createCourse(user, course));
    }

    @PostMapping("/delete/{id}")
    public CourseDTO deleteCourse(@PathVariable long id){
        return courseMapper.fromCourseToDTO(service.deleteCourse(id));
    }

    @GetMapping("/{courseId}/pass/{userId}")
    public boolean isStudentPassCourse(@PathVariable long courseId, @PathVariable long userId){
        return service.isStudentPassCourse(userId, courseId);
    }

    @GetMapping("/{courseId}/student/{username}/rate/{rating}")
    public CourseDTO rateCourse(@PathVariable long courseId, @PathVariable String username,@PathVariable int rating){
        return courseMapper.fromCourseToDTO(service.rateCourse(courseId, username, rating));
    }

    @GetMapping("/progress/{courseId}/{username}")
    public int getProgress(@PathVariable long courseId, @PathVariable String username){
        return service.getCourseProgress(courseId, username);
    }
}
