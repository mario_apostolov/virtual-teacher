package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.models.*;
import com.craftcap.virtualteacher.models.dtos.NewHomeworkDTO;
import com.craftcap.virtualteacher.models.dtos.NewLectureDTO;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import com.craftcap.virtualteacher.services.contracts.LectureService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping
public class LecturePageController {
    private LectureService lectureService;
    private CourseService courseService;
    private UserService userService;
    private HomeworkService homeworkService;

    @Autowired
    public LecturePageController(LectureService lectureService, CourseService courseService, UserService userService, HomeworkService homeworkService) {
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.userService = userService;
        this.homeworkService = homeworkService;
    }

    @GetMapping("/courses/{courseId}/new-lecture")
    @PreAuthorize("hasRole('TEACHER')")
    public String showPageNewLecture(@PathVariable long courseId, Model model) {
        Course course = courseService.getUnfinishedById(courseId);
        model.addAttribute("lecture", new NewLectureDTO());
        model.addAttribute("course", course);
        return "new-lecture";
    }

    @PostMapping("/courses/{courseId}/new-lecture")
    @PreAuthorize("hasRole('TEACHER')")
    public String createLecture(@Valid @ModelAttribute NewLectureDTO newLectureDTO, BindingResult bindingResult, Model model, @PathVariable long courseId) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
            model.addAttribute("lecture", newLectureDTO);
            return "new-lecture";
        }
        lectureService.createLecture(newLectureDTO, courseId);
        return String.format("redirect:/courses/%d/unfinished", courseId);
    }

    @GetMapping("/courses/{courseId}/lectures/{lectureId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public String showPageLecture(Principal principal, @PathVariable long lectureId, Model model, @PathVariable long courseId) {
        Lecture lecture = lectureService.getLectureById(lectureId);
        Lecture current = lectureService.getCurrentLectureByHomework(courseId, principal.getName());
        User user = userService.getByUsername(principal.getName());
        lectureService.isLectureIsDisplayable(principal.getName(), lectureId, courseId);
        model.addAttribute("lecture", lecture);
        model.addAttribute("homework", new NewHomeworkDTO());
        if (current != null) {
            if (current.getId() == lecture.getId()) {
                if (lectureService.getFinishedLecturesByCourseByUser(courseId, user.getUsername()).stream().anyMatch(lecture1 -> lecture1.getId() == lectureId)) {
                    model.addAttribute("finished", true);
                } else {
                    model.addAttribute("continue", true);
                }
            }
        }
        return "lecture";
    }

    @PostMapping("/courses/{courseId}/lectures/{lectureId}/add-homework")
    @PreAuthorize("hasRole('USER')")
    public String uploadHomework(Principal principal, @ModelAttribute NewHomeworkDTO newHomeworkDTO, @PathVariable long courseId, @PathVariable long lectureId) {
        homeworkService.createHomework(principal.getName() ,courseId, lectureId, newHomeworkDTO);
        return String.format("redirect:/courses/%d/lectures/%d", courseId, lectureId);
    }

    @PostMapping("/courses/{courseId}/lectures/{lectureId}/finish")
    @PreAuthorize("hasRole('USER')")
    public String finishLecture(Principal principal, @PathVariable long courseId, @PathVariable long lectureId, @ModelAttribute Homework homework) {
        User user = userService.getByUsername(principal.getName());
        Lecture lecture = lectureService.getLectureById(lectureId);
        List<Lecture> lectures = lectureService.getFinishedLecturesByCourseByUser(courseId, user.getUsername());
        lectures.add(lecture);
        user.setFinishedLectures(lectures);
        userService.updateUser(user);
        return String.format("redirect:/courses/%d/lectures/%d", courseId, lectureId);
    }
}
