package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.models.dtos.EmailDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ContactController {

    @GetMapping("/contact")
    public String showContactpage(Model model){
        model.addAttribute("email", new EmailDTO());
        return "contact";
    }
}
