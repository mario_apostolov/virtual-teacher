package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.models.dtos.HomeworkDTO;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
@PreAuthorize("hasRole('TEACHER')")
public class TeacherController {
    private HomeworkService homeworkService;
    private UserService userService;

    @Autowired
    public TeacherController(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    @PostMapping("/teachers/homework/evaluate")
    public String evaluateHomework(@ModelAttribute HomeworkDTO homeworkDTO, Principal principal){
        Homework homework = homeworkService.getById(homeworkDTO.getId());
        homework.setGrade(homeworkDTO.getGrade());
        homeworkService.evaluateHomework(homework);
        return String.format("redirect:/users/%s", principal.getName());
    }
}
