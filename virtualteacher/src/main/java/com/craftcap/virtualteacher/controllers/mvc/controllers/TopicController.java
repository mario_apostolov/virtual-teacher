package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.models.dtos.TopicDTO;
import com.craftcap.virtualteacher.models.mappers.TopicMapper;
import com.craftcap.virtualteacher.services.contracts.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping
public class TopicController {
    private TopicService topicService;
    private TopicMapper topicMapper;

    @Autowired
    public TopicController(TopicService topicService, TopicMapper topicMapper) {
        this.topicService = topicService;
        this.topicMapper = topicMapper;
    }

    @GetMapping("/topics")
    public String showTopicsPage(Model model){
        List<Topic> topics  = topicService.getAll();
        List<TopicDTO> dtos = topicMapper.fromTopicsToDTOS(topics);
        model.addAttribute("topics", dtos);
        return "topics";
    }
}
