package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.Amazon.S3Services;
import com.craftcap.virtualteacher.Tika.TikaAnalysis;
import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.exceptions.WrongFIleFormat;
import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.*;
import com.craftcap.virtualteacher.models.mappers.CourseHomeworkMapper;
import com.craftcap.virtualteacher.models.mappers.CourseLiteMapper;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping
public class UserController {
    private CourseService courseService;
    private UserService userService;
    private CourseLiteMapper courseLiteMapper;
    private HomeworkService homeworkService;
    private CourseHomeworkMapper courseHomeworkMapper;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private S3Services s3Services;

    @Autowired
    public UserController(CourseService courseService, UserService userService, CourseLiteMapper courseLiteMapper, HomeworkService homeworkService, CourseHomeworkMapper courseHomeworkMapper, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, S3Services s3Services) {
        this.courseService = courseService;
        this.userService = userService;
        this.courseLiteMapper = courseLiteMapper;
        this.homeworkService = homeworkService;
        this.courseHomeworkMapper = courseHomeworkMapper;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.s3Services = s3Services;
    }

    @PostMapping("/courses/{courseId}/enroll/{username}")
    public String enrollToCourse(@PathVariable String username, @PathVariable long courseId) {
        userService.enrollToCourse(username, courseId);
        return String.format("redirect:/courses/%d/lectures", courseId);
    }

    @GetMapping("/users/{username}")
    public String showProfilePage(Principal principal, Model model, @PathVariable String username) {
        User user = userService.getByUsername(username);
        model.addAttribute("user", user);
        List<CourseLiteDTO> enrolledCourses = courseLiteMapper
                .fromCoursesToDTOS(courseService.getEnrolledCoursesByStudent(username));
        model.addAttribute("enrolledCourses", enrolledCourses);
        List<CourseLiteDTO> completedCourses = courseLiteMapper
                .fromCoursesToDTOS(courseService.getCompletedCoursesByStudent(username));
        model.addAttribute("completedCourses", completedCourses);
        if (user.getRole() == Role.TEACHER) {
            List<CourseLiteDTO> createdCourses = courseLiteMapper
                    .fromCoursesToDTOS(courseService.getCreatedCourses(username));
            model.addAttribute("createdCourses", createdCourses);
            List<CourseHomeworkDTO> courseHomeworkDTOS = courseHomeworkMapper
                    .fromCoursesToDTOS(courseService.getCreatedCourses(username));
            model.addAttribute("newHomework", new HomeworkDTO());
            model.addAttribute("coursesHomework", courseHomeworkDTOS);
        }
        return "profile";
    }


    @GetMapping("/users/{username}/change-pass")
    @PreAuthorize("hasRole('USER')")
    public String changePasswordPage(Principal principal, Model model, @PathVariable String username) {
        model.addAttribute("pass", true);
        UserNewPasswordDTO userNewPasswordDTO = new UserNewPasswordDTO();
        userNewPasswordDTO.setUsername(principal.getName());
        model.addAttribute("user", userNewPasswordDTO);
        return "profile-edit";
    }

    @PostMapping("/users/{username}/change-pass")
    @PreAuthorize("hasRole('USER')")
    public String changePass(Principal principal, @Valid @ModelAttribute UserNewPasswordDTO userNewPasswordDTO, @PathVariable String username, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
            model.addAttribute("pass", true);
            userNewPasswordDTO.setPassword(null);
            userNewPasswordDTO.setOldPassword(null);
            userNewPasswordDTO.setRepeat(null);
            model.addAttribute("user", userNewPasswordDTO);
            return "profile-edit";
        }
        User user = userService.getByUsername(principal.getName());
        userDetailsManager.changePassword(userNewPasswordDTO.getOldPassword(), passwordEncoder.encode(userNewPasswordDTO.getPassword()));
        user.setPassword(userNewPasswordDTO.getPassword());
        return String.format("redirect:/users/%s", username);
    }

    @GetMapping("/users/{username}/edit")
    @PreAuthorize("hasRole('USER')")
    public String editProfilePage(Principal principal, Model model, @PathVariable String username) {
        model.addAttribute("edit", true);
        User user = userService.getByUsername(principal.getName());
        UserEditDTO userEditDTO = new UserEditDTO();
        userEditDTO.setUsername(username);
        userEditDTO.setEmail(user.getEmail());
        userEditDTO.setFirstName(user.getFirstName());
        userEditDTO.setLastName(user.getLastName());
        model.addAttribute("user", userEditDTO);
        return "profile-edit";
    }

    @PostMapping("/users/{username}/edit")
    @PreAuthorize("hasRole('USER')")
    public String editProfile(@Valid @ModelAttribute UserEditDTO userEditDTO, BindingResult bindingResult, Model model, Principal principal,  @PathVariable String username) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
            model.addAttribute("edit", true);
            model.addAttribute("user", userEditDTO);
            return "profile-edit";
        }
        User user = userService.getByUsername(principal.getName());
        user.setEmail(userEditDTO.getEmail());
        user.setFirstName(userEditDTO.getFirstName());
        user.setLastName(userEditDTO.getLastName());
        if (!userEditDTO.getFile().isEmpty()) {
            String pictureFile = "";
            String photoFileName = userEditDTO.getUsername();
            TikaAnalysis.isFileCorrectSize(userEditDTO.getFile(), AppConstants.IMAGE_MAX_SIZE);
            try {
                InputStream inputStream = new BufferedInputStream(userEditDTO.getFile().getInputStream());
                pictureFile = TikaAnalysis.detectDocTypeUsingDetector(inputStream);
                TikaAnalysis.checkImageType(pictureFile);
                s3Services.uploadFile(photoFileName, userEditDTO.getFile());
            } catch (IOException e) {
                throw new WrongFIleFormat(e.getMessage());
            }
            user.setPicture(photoFileName);
        }
        userService.updateUser(user);
        return String.format("redirect:/users/%s", username);
    }

}
