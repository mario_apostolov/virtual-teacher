package com.craftcap.virtualteacher.controllers.rest;


import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.models.dtos.CourseDTO;
import com.craftcap.virtualteacher.models.dtos.TopicDTO;
import com.craftcap.virtualteacher.models.mappers.CourseMapper;
import com.craftcap.virtualteacher.models.mappers.TopicMapper;
import com.craftcap.virtualteacher.services.contracts.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/topics")
public class TopicRestController {
    private TopicService service;
    private CourseMapper courseMapper;
    private TopicMapper topicMapper;

    @Autowired
    public TopicRestController(TopicService service, CourseMapper courseMapper, TopicMapper topicMapper) {
        this.service = service;
        this.courseMapper = courseMapper;
        this.topicMapper = topicMapper;
    }

    @GetMapping
    public List<TopicDTO> showAllTopics() {
        return service.getAll()
                .stream()
                .map(topic -> topicMapper.fromTopicToDTO(topic))
                .collect(Collectors.toList());
    }

    @GetMapping("/{name}")
    public List<CourseDTO> getCoursesByTopic(@PathVariable String name){
        return service.getAllCoursesByTopic(name)
                .stream()
                .map(course -> courseMapper.fromCourseToDTO(course))
                .collect(Collectors.toList());
    }

    @PostMapping("/create")
    public TopicDTO createTopic(@Valid @RequestBody Topic topic){
        return topicMapper.fromTopicToDTO(service.createTopic(topic.getTopic()));
    }
}