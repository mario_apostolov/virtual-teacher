package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.models.dtos.CourseLiteDTO;
import com.craftcap.virtualteacher.models.mappers.CourseLiteMapper;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping
public class HomePageController {
    private CourseService courseService;
    private CourseLiteMapper courseLiteMapper;

    @Autowired
    public HomePageController(CourseService courseService, CourseLiteMapper courseLiteMapper) {
        this.courseService = courseService;
        this.courseLiteMapper = courseLiteMapper;
    }

    @GetMapping("/")
    public String homePage(Model model) {
        List<CourseLiteDTO> topThreeCourses = courseLiteMapper.fromCoursesToDTOS(courseService.getTopThreeCoursesByRating());
        model.addAttribute("topThreeCourses", topThreeCourses);
        return "index";
    }
}