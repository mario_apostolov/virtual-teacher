package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.Amazon.S3Services;
import com.craftcap.virtualteacher.models.*;
import com.craftcap.virtualteacher.models.dtos.CourseLiteDTO;
import com.craftcap.virtualteacher.models.dtos.LectureDTO;
import com.craftcap.virtualteacher.models.dtos.NewCourseDTO;
import com.craftcap.virtualteacher.models.mappers.CourseLiteMapper;
import com.craftcap.virtualteacher.models.mappers.LectureMapper;
import com.craftcap.virtualteacher.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping
public class CoursePageController {
    @Value("${jsa.s3.uploadfile}")
    private String uploadFilePath;
    private S3Services s3Services;
    private UserService userService;
    private CourseService service;
    private LectureService lectureService;
    private CourseLiteMapper courseLiteMapper;
    private LectureMapper lectureMapper;
    private SearchService searchService;
    private RatingService ratingService;
    private HomeworkService homeworkService;
@Autowired
    public CoursePageController(S3Services s3Services, UserService userService, CourseService service, LectureService lectureService, CourseLiteMapper courseLiteMapper, LectureMapper lectureMapper, SearchService searchService, RatingService ratingService, HomeworkService homeworkService) {
        this.s3Services = s3Services;
        this.userService = userService;
        this.service = service;
        this.lectureService = lectureService;
        this.courseLiteMapper = courseLiteMapper;
        this.lectureMapper = lectureMapper;
        this.searchService = searchService;
        this.ratingService = ratingService;
        this.homeworkService = homeworkService;
    }



    @GetMapping("/courses")
    public String showCoursesPage(Model model, @RequestParam(required = false) Integer page,
                                  @RequestParam(required = false) String sort) {
        Page<Course> courses = service.getAllCourses(page, sort);
        model = setAttributes(model, page, sort, courses);
        return "courses";
    }

    @GetMapping("/courses/search")
    public String showCoursesPageSearch(Model model, @RequestParam(required = false) Integer page,
                                        @RequestParam(required = false) String sort, @RequestParam String field) {
        Page<Course> courses = searchService.searchCourses(field, page, sort);
        model = setAttributes(model, page, sort, courses);
        return "courses";
    }

    @GetMapping("/courses/filter")
    public String showCoursesPageFiltered(Model model, @RequestParam(required = false) Integer page,
                                          @RequestParam(required = false) String sort, @RequestParam(required = false) String title,
                                          @RequestParam(required = false) String topic, @RequestParam(required = false) String teacher) {
        Page<Course> courses = searchService.filteredCourses(page, sort, title, topic, teacher);
        model = setAttributes(model, page, sort, courses);
        return "courses";
    }

    @GetMapping("/courses/new-course")
    @PreAuthorize("hasRole('TEACHER')")
    public String showNewCoursesPage(Principal principal, Model model) throws InterruptedException {
        User teacher = userService.getUserByRoleAndUsername(Role.TEACHER, principal.getName());
        model.addAttribute("teacher", teacher);
        model.addAttribute("course", new NewCourseDTO());
        return "new-course";
    }

    @PostMapping("/courses/new-course")
    @PreAuthorize("hasRole('TEACHER')")
    public String createCourse(@Valid @ModelAttribute NewCourseDTO newCourse, BindingResult bindingResult, Model model, Principal principal) {
        if(bindingResult.hasErrors()){
            model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
            model.addAttribute("course", newCourse);
            return "new-course";
        }
        User teacher = userService.getUserByRoleAndUsername(Role.TEACHER, principal.getName());
        Course course = service.createCourse(teacher,newCourse);
        return String.format("redirect:/courses/%d/unfinished", course.getId());
    }

    @GetMapping("/courses/{courseId}/lectures")
    public String showPageLectures(Principal principal, @PathVariable long courseId, Model model) {
        CourseLiteDTO courseLiteDTO = courseLiteMapper.fromCourseToDTO(service.getById(courseId));
        List<LectureDTO> lectures = lectureMapper.fromLecturesToDTOS(lectureService.getAllByCourse(courseId));
        model.addAttribute("course", courseLiteDTO);
        model.addAttribute("lectures", lectures);
        //finished
        // guest
        if (principal == null) {
            model.addAttribute("guest", true);
        } else {
            User user = userService.getByUsername(principal.getName());
            courseLiteDTO.setGrade(homeworkService.getAvgGradeByCourseByStudent(user.getId(), courseId));
            // not enrolled
            if (courseLiteDTO.getCreatorUsername().equals(principal.getName())) {
                model.addAttribute("teacher", true);
            } else if(!service.existByStudents(user, courseId)){
                model.addAttribute("notEnrolled", true);
            } else {
                // enrolled student
                int progress = service.getCourseProgress(courseId, user.getUsername());
                model.addAttribute("enrolled", true);
                model.addAttribute("progress", progress);
                model.addAttribute("unfinished", lectureMapper.fromLecturesToDTOS(lectureService.getUnfinishedLecturesByHomeworkByUser(courseId, user.getUsername())));
                model.addAttribute("unfinished", lectureMapper.fromLecturesToDTOS(
                        lectureService.getUnfinishedLecturesByHomeworkByUser(courseId, user.getUsername())));
                model.addAttribute("finished", lectureMapper.fromLecturesToDTOS(
                        lectureService.getFinishedLecturesByHomeworkByUser(courseId, user.getUsername()), user));

                if (lectureService.getCurrentLectureByHomework(courseId, user.getUsername()) == null) {
                    model.addAttribute("noCurrent", true);
                    if(service.isStudentPassCourse(user.getId(), courseId)){
                        if(!ratingService.isCourseRatedByUser(service.getById(courseId), user)){
                            model.addAttribute("rating", new Rating());
                            model.addAttribute("notRated", true);
                        }
                    }
                } else {
                    model.addAttribute("current", lectureMapper.fromLectureToDTO(
                            lectureService.getCurrentLectureByHomework(courseId, user.getUsername())));
                }
            }
        }
        return "lectures";
    }

    @GetMapping("/courses/{courseId}/unfinished")
    @PreAuthorize("hasRole('TEACHER')")
    public String showUnfinishedCoursePage(Principal principal, @PathVariable long courseId, Model model) {
        userService.isUserIsCreator(principal.getName(), courseId);
        CourseLiteDTO courseLiteDTO = courseLiteMapper.fromCourseToDTO(service.getUnfinishedById(courseId));
        List<LectureDTO> lectures = lectureMapper.fromLecturesToDTOS(lectureService.getUnfinishedCourseLecturesById(courseId));
        model.addAttribute("course", courseLiteDTO);
        model.addAttribute("lectures", lectures);
        return "unfinished-course";
    }

    @PostMapping("/courses/{courseId}/submit")
    @PreAuthorize("hasRole('TEACHER')")
    public String submitCourse(@PathVariable long courseId) {
        service.setCourseToFinished(courseId);
        return String.format("redirect:/courses/%d/lectures", courseId);
    }

    @PostMapping("/courses/{courseId}/rate")
    @PreAuthorize("hasRole('USER')")
    public String rateCourse(@Valid @ModelAttribute Rating rating, BindingResult bindingResult,@PathVariable long courseId, Principal principal) {
        service.rateCourse(courseId, principal.getName(), rating.getRating());
        return String.format("redirect:/courses/%d/lectures", courseId);
    }

    @GetMapping("/courses/{courseId}/edit")
    @PreAuthorize("hasRole('TEACHER')")
    public String showEditCoursePage(Model model, Principal principal, @PathVariable long courseId){
        userService.isUserIsCreator(principal.getName(), courseId);
        model.addAttribute("course", courseLiteMapper.fromCourseToDTO(service.getById(courseId)));
        model.addAttribute("lectures", lectureService.getAllByCourse(courseId));
        return "course-edit";
    }

    @PostMapping("/courses/{courseId}/edit")
    @PreAuthorize("hasRole('TEACHER')")
    public String editCourse(@ModelAttribute CourseLiteDTO courseLiteDTO, BindingResult bindingResult, Model model, @PathVariable long courseId){
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError().getDefaultMessage());
            model.addAttribute("course", courseLiteDTO);
            return "profile-edit";
        }
        service.updateCourse(courseId, courseLiteDTO.getDescription());
        return String.format("redirect:/courses/%d/lectures",courseId);
    }

    private Model setAttributes(Model model, Integer page, String sort, Page<Course> courses){
        if (page == null) {
            page = 1;
        }
        if (sort == null) {
            sort = "id";
        }
        model.addAttribute("pages", service.getPages(courses));
        model.addAttribute("sort", sort);
        model.addAttribute("currentPage", page);
        List<CourseLiteDTO> dtos = courseLiteMapper.fromCoursesToDTOS(courses.getContent());
        model.addAttribute("courses", dtos);
        return model;
    }
}
