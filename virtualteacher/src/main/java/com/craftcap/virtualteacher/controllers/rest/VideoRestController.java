package com.craftcap.virtualteacher.controllers.rest;

import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.services.contracts.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;

@RestController
public class VideoRestController {
    private LectureService lectureService;

    @Autowired
    public VideoRestController(LectureService lectureService) {
        this.lectureService = lectureService;
    }

    @GetMapping(value = "/video", produces = "video/mp4")
    @ResponseBody
    public FileSystemResource videoSource(@RequestParam(value = "id", required = true) int id) {
        Lecture lecture = lectureService.getLectureById(id);
        return new FileSystemResource(new File(String.format(AppConstants.VIDEO_DIRECTORY + "%s", lecture.getVideo())));
    }
}
