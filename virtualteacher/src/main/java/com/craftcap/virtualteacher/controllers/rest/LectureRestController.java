package com.craftcap.virtualteacher.controllers.rest;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.dtos.LectureDTO;
import com.craftcap.virtualteacher.models.dtos.NewLectureDTO;
import com.craftcap.virtualteacher.models.mappers.LectureMapper;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/courses")
public class LectureRestController {
    private LectureService lectureService;
    private LectureMapper lectureMapper;
    private CourseService courseService;

    @Autowired
    public LectureRestController(LectureService lectureService, LectureMapper lectureMapper, CourseService courseService){
        this.lectureService = lectureService;
        this.lectureMapper = lectureMapper;
        this.courseService = courseService;
    }

    @GetMapping("/{courseId}/lectures")
    public List<LectureDTO> getAllLecturesByCourse(@PathVariable long courseId){
        try {
            return lectureService.getAllByCourse(courseId)
                    .stream()
                    .map(lecture -> lectureMapper.fromLectureToDTO(lecture))
                    .collect(Collectors.toList());
        }catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{courseId}/lectures/{id}")
    public LectureDTO getLectureById(@PathVariable long id){
        return lectureMapper.fromLectureToDTO(lectureService.getLectureById(id));
    }

    @PostMapping("/{courseId}/new-lecture")
    public LectureDTO createLecture(@Valid @RequestBody NewLectureDTO lectureDTO, @PathVariable long courseId){
        return lectureMapper.fromLectureToDTO(lectureService.createLecture(lectureDTO, courseId));
    }

    @PostMapping("/{courseId}/lectures/delete/{id}")
    public LectureDTO deleteLecture(@PathVariable long courseId, @PathVariable long id){
        return lectureMapper.fromLectureToDTO(lectureService.deleteLecture(courseId ,id));
    }

    @GetMapping("/{courseId}/lectures/finished/{username}")
    public List<LectureDTO> getFinishedLecturesByCourseByUser(@PathVariable long courseId, @PathVariable String username){
        return lectureService.getFinishedLecturesByCourseByUser(courseId, username)
                .stream()
                .map(lecture -> lectureMapper.fromLectureToDTO(lecture))
                .collect(Collectors.toList());
    }

    @GetMapping("/{courseId}/lectures/current/{username}")
    public LectureDTO getCurrentLecture(@PathVariable long courseId, @PathVariable String username){
        return lectureMapper.fromLectureToDTO(lectureService.getCurrentLectureByHomework(courseId, username));
    }

    @GetMapping("/{courseId}/lectures/unfinished/{username}")
    public List<LectureDTO> getUnfinishedLecturesByCourseByUser(@PathVariable long courseId, @PathVariable String username){
        return lectureService.getUnfinishedLecturesByHomeworkByUser(courseId, username)
                .stream()
                .map(lecture -> lectureMapper.fromLectureToDTO(lecture))
                .collect(Collectors.toList());
    }
}
