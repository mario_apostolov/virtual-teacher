package com.craftcap.virtualteacher.controllers.mvc.controllers;

import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewAdminDTO;
import com.craftcap.virtualteacher.services.contracts.AdminService;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping
public class AdminPageController {
    private AdminService adminService;
    private UserService userService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private CourseService courseService;

    @Autowired
    public AdminPageController(AdminService adminService, UserService userService, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, CourseService courseService) {
        this.adminService = adminService;
        this.userService = userService;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.courseService = courseService;
    }

    @GetMapping("/admin")
    public String showAdminPage(Model model, Principal principal) {
        if (principal == null) {
            return "login";
        } else if (principal.getName().equals(userService.getUserByRoleAndUsername(Role.ADMIN, principal.getName()).getUsername())) {
            model.addAttribute("newAdmin", new NewAdminDTO());
            model.addAttribute("courses", courseService.getAllCourses());
            model.addAttribute("students", adminService.getAllEnabledStudents());
            model.addAttribute("teachers", adminService.getAllEnabledTeachers());
            model.addAttribute("waiting", adminService.getAllEnabledWaiting());
            model.addAttribute("admins", adminService.getAllEnabledAdmin());
            return "admin";
        } else {
            return "login";
        }
    }

    @PostMapping("/admin/approve/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public String changeRoleToTeacher(@PathVariable String username) {
        User user = userService.getByUsername(username);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_TEACHER", "ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        user.getPassword(),
                        authorities);
        userDetailsManager.updateUser(newUser);
        user.setRole(Role.TEACHER);
        userService.updateUser(user);
        return "redirect:/admin";
    }

    @PostMapping("/admin/reject/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public String rejectTeacher(@PathVariable String username) {
        User user = userService.getByUsername(username);
        user.setRole(Role.STUDENT);
        userService.updateUser(user);
        return "redirect:/admin";
    }

    @PostMapping("/admin/create")
    @PreAuthorize("hasRole('ADMIN')")
    public String createAdmin(@ModelAttribute NewAdminDTO admin) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_ADMIN");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        admin.getUsername(),
                        passwordEncoder.encode(admin.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);
        User user = userService.getByUsername(admin.getUsername());
        user.setRole(Role.ADMIN);
        userService.updateUser(user);
        return "redirect:/admin";
    }

    @PostMapping("/admin/delete/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteUser(@PathVariable String username) {
        userService.deleteUser(username);
        return "redirect:/admin";
    }

    @PostMapping("/admin/delete/course/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public String deleteUser(@PathVariable long id) {
        courseService.deleteCourse(id);
        return "redirect:/admin";
    }
}
