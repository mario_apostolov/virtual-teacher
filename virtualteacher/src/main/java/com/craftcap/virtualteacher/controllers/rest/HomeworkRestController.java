package com.craftcap.virtualteacher.controllers.rest;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.CourseHomeworkDTO;
import com.craftcap.virtualteacher.models.dtos.HomeworkDTO;
import com.craftcap.virtualteacher.models.mappers.CourseHomeworkMapper;
import com.craftcap.virtualteacher.models.mappers.HomeworkMapper;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class HomeworkRestController {
    private CourseService courseService;
    private CourseHomeworkMapper courseHomeworkMapper;
    private UserService userService;
    private HomeworkService homeworkService;
    private HomeworkMapper homeworkMapper;

    @Autowired
    public HomeworkRestController(HomeworkMapper homeworkMapper, HomeworkService homeworkService, UserService userService, CourseHomeworkMapper courseHomeworkMapper, CourseService courseService) {
        this.homeworkMapper = homeworkMapper;
        this.homeworkService = homeworkService;
        this.userService = userService;
        this.courseHomeworkMapper = courseHomeworkMapper;
        this.courseService = courseService;
    }

    @GetMapping("/api/teachers/{teacherId}/homework")
    public List<CourseHomeworkDTO> getHomeworkByTeacher(Principal principal,@PathVariable long teacherId){
        User user = userService.getUserByRoleAndUsername(Role.TEACHER, principal.getName());
        List<Course> courses = courseService.getCoursesByTeacherId(teacherId);
        List<CourseHomeworkDTO> dtos =  courses
                .stream()
                .map(course -> courseHomeworkMapper.fromCourseToDTO(course))
                .collect(Collectors.toList());
        return dtos;
    }

    @PostMapping("/api/teachers/{teacherId}/homeworks/{homeworkId}/evaluate")
    public HomeworkDTO evaluateHomework(@PathVariable long teacherId,@PathVariable long homeworkId, @RequestBody int grade){
        Homework homework = homeworkService.getById(homeworkId);
        homework.setGrade(grade);
        return homeworkMapper.fromHomeworkToDTO(homeworkService.evaluateHomework(homework));
    }

    @GetMapping("/api/{courseId}/avg-grade/{userId}")
    public int getAvgGradeByCourseByStudent(@PathVariable long userId,@PathVariable long courseId){
        return homeworkService.getAvgGradeByCourseByStudent(userId, courseId);
    }

}
