package com.craftcap.virtualteacher.controllers.rest;

import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController
@RequestMapping("/images/")
public class ImageRestController {
    private UserService userService;

    @Autowired
    public ImageRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/{username}")
    public ResponseEntity<byte[]> getUserImage(@PathVariable String username) throws IOException {
        User user = userService.getByUsername(username);
        File file = new File(AppConstants.PHOTOS_DIRECTORY + user.getPicture());
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        return new ResponseEntity<>(Files.readAllBytes(file.toPath()), headers, HttpStatus.OK);
    }

    @GetMapping("/courses/{filename}")
    public ResponseEntity<byte[]> getCourseImage(@PathVariable String filename) throws IOException {
        File file = new File(AppConstants.PHOTOS_DIRECTORY + filename);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        return new ResponseEntity<>(Files.readAllBytes(file.toPath()), headers, HttpStatus.OK);
    }
}