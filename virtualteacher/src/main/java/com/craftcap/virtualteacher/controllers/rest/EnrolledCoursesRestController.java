package com.craftcap.virtualteacher.controllers.rest;

import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.CourseDTO;
import com.craftcap.virtualteacher.models.dtos.UserDTO;
import com.craftcap.virtualteacher.models.mappers.CourseMapper;
import com.craftcap.virtualteacher.models.mappers.UserMapper;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class EnrolledCoursesRestController {
    private UserService userService;
    private UserMapper userMapper;
    private CourseMapper courseMapper;
    private CourseService courseService;

    @Autowired
    public EnrolledCoursesRestController(UserMapper userMapper, UserService userService,
                                         CourseMapper courseMapper, CourseService courseService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.courseMapper = courseMapper;
        this.courseService = courseService;
    }

    @PostMapping("/api/courses/{courseId}/enroll/{username}")
    public UserDTO enrollToCourse(@PathVariable String username, @PathVariable long courseId){
        User user = userService.enrollToCourse(username, courseId);
        return userMapper.fromUserToDTO(user);
    }

    @GetMapping("api/enrolled/{username}")
    public List<CourseDTO> getEnrolledCourses(@PathVariable String username){
        return courseService.getEnrolledCoursesByStudent(username)
                .stream()
                .map(course -> courseMapper.fromCourseToDTO(course))
                .collect(Collectors.toList());
    }
}
