package com.craftcap.virtualteacher.models.mappers;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.dtos.CourseHomeworkDTO;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseHomeworkMapper {
    private HomeworkService homeworkService;
    private HomeworkMapper mapper;

    @Autowired
    public CourseHomeworkMapper(HomeworkMapper mapper, HomeworkService homeworkService) {
        this.mapper = mapper;
        this.homeworkService = homeworkService;
    }

    public CourseHomeworkDTO fromCourseToDTO(Course course) {
        CourseHomeworkDTO dto = new CourseHomeworkDTO();
        dto.setId(course.getId());
        dto.setTitle(course.getTitle());
        dto.setHomework(homeworkService.getAllByCourseId(course.getId())
                .stream()
                .map(homework -> mapper.fromHomeworkToDTO(homework))
                .collect(Collectors.toList()));
        return dto;
    }

    public List<CourseHomeworkDTO> fromCoursesToDTOS(List<Course> courses) {
        List<CourseHomeworkDTO> dtos = new ArrayList<>();
        for (Course course : courses) {
            CourseHomeworkDTO courseHomeworkDTO = fromCourseToDTO(course);
            if (courseHomeworkDTO.getHomework().size() != 0){
                dtos.add(courseHomeworkDTO);
            }
        }
        return dtos;
    }
}

