package com.craftcap.virtualteacher.models.dtos;

import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.password.validation.ValidPassword;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class NewUserDTO {
    @Size(min = 4, max = 50, message = "Username should be between 4 and 50 characters.")
    private String username;
    @ValidPassword
    private String password;
    @Email(message = "Email should be valid")
    private String email;
    private MultipartFile file;
    @Size(min = 1, max = 50, message = "First name should be between 1 and 50 characters.")
    private String firstName;
    @Size(min = 1, max = 50, message = "Last name should be between 1 and 50 characters.")
    private String lastName;
    private Role role;

    public NewUserDTO() {
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
