package com.craftcap.virtualteacher.models.dtos;

import com.craftcap.virtualteacher.models.password.validation.ValidPassword;

public class UserNewPasswordDTO {
    @ValidPassword
    private String oldPassword;
    private String username;
    @ValidPassword
    private String password;
    @ValidPassword
    private String repeat;

    public UserNewPasswordDTO() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeat() {
        return repeat;
    }

    public void setRepeat(String repeat) {
        this.repeat = repeat;
    }
}
