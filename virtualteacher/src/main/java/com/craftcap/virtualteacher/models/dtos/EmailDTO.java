package com.craftcap.virtualteacher.models.dtos;

import javax.validation.constraints.Email;

public class EmailDTO {
    String name;
    @Email
    String email;
    String body;

    public EmailDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
