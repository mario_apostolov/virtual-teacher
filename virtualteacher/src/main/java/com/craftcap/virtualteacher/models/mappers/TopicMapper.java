package com.craftcap.virtualteacher.models.mappers;

import com.craftcap.virtualteacher.databaselayer.CourseRepository;
import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.models.dtos.TopicDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TopicMapper {
    private CourseLiteMapper courseMapper;
    private CourseRepository courseRepository;

    @Autowired
    public TopicMapper(CourseLiteMapper courseMapper, CourseRepository courseRepository) {
        this.courseMapper = courseMapper;
        this.courseRepository = courseRepository;
    }


    public TopicDTO fromTopicToDTO(Topic topic){
        TopicDTO dto = new TopicDTO();
        dto.setTopic(topic.getTopic());
        dto.setCourses(courseRepository.getAllByTopicAndFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc(topic)
                        .stream()
                        .map(course -> courseMapper.fromCourseToDTO(course))
                        .collect(Collectors.toList()));
        return dto;
    }

    public List<TopicDTO> fromTopicsToDTOS(List<Topic> topics){
        List<TopicDTO> dtos = new ArrayList<>();
        for (Topic topic: topics) {
            TopicDTO dto = fromTopicToDTO(topic);
            if (dto.getCourses().size() != 0)
                dtos.add(dto);
        }
        return dtos;
    }
}
