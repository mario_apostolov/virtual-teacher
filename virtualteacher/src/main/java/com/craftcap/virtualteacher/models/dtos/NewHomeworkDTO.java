package com.craftcap.virtualteacher.models.dtos;

import org.springframework.web.multipart.MultipartFile;


public class NewHomeworkDTO {
    private MultipartFile homeworkFile;

    public NewHomeworkDTO() {
    }

    public MultipartFile getHomeworkFile() {
        return homeworkFile;
    }

    public void setHomeworkFile(MultipartFile homeworkFile) {
        this.homeworkFile = homeworkFile;
    }

}
