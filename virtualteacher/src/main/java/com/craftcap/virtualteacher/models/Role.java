package com.craftcap.virtualteacher.models;

public enum Role {
    STUDENT,
    ADMIN,
    TEACHER,
    WAITING
}
