package com.craftcap.virtualteacher.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.validation.constraints.Size;

public class NewLectureDTO {
    @Size(min = 3, max = 255, message = "Lecture title should be between 3 and 255 characters")
    private String title;
    private String description;
    private MultipartFile videoFile;
    private MultipartFile assignmentFile;

    public NewLectureDTO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile getVideoFile() {
        return videoFile;
    }

    public void setVideoFile(MultipartFile videoFile) {
        this.videoFile = videoFile;
    }

    public MultipartFile getAssignmentFile() {
        return assignmentFile;
    }

    public void setAssignmentFile(MultipartFile assignmentFile) {
        this.assignmentFile = assignmentFile;
    }
}
