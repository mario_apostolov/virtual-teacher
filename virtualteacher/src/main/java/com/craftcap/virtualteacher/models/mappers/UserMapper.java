package com.craftcap.virtualteacher.models.mappers;

import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.UserDTO;
import org.hibernate.internal.build.AllowPrintStacktrace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UserMapper {
    private CourseLiteMapper mapper;

    @Autowired
    public UserMapper(CourseLiteMapper mapper) {
        this.mapper = mapper;
    }

    public UserDTO fromUserToDTO(User user){
        UserDTO dto = new UserDTO();
       dto.setFirstName(user.getFirstName());
       dto.setLastName(user.getLastName());
       dto.setEnrolledCourses(user.getEnrolledCourses()
               .stream()
               .map(course -> mapper.fromCourseToDTO(course))
               .collect(Collectors.toList()));
        return dto;
    }
}
