package com.craftcap.virtualteacher.models.mappers;

import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.models.dtos.HomeworkDTO;
import org.springframework.stereotype.Service;

@Service
public class HomeworkMapper {

    public HomeworkDTO fromHomeworkToDTO(Homework homework){
        HomeworkDTO dto = new HomeworkDTO();
        dto.setStudent(String.format("%s, %s",homework.getStudent().getLastName(),homework.getStudent().getFirstName()));
        dto.setLectureId(homework.getLecture().getId());
        dto.setId(homework.getId());
        dto.setLectureTitle(homework.getLecture().getTitle());
        dto.setHomework(homework.getHomework());
        dto.setGrade(homework.getGrade());
        return dto;
    }
}
