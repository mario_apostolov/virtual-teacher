package com.craftcap.virtualteacher.models.dtos;

import com.craftcap.virtualteacher.models.Homework;

import java.util.List;

public class CourseHomeworkDTO {
    private long id;
    private String title;
    private List<HomeworkDTO> homework;

    public CourseHomeworkDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<HomeworkDTO> getHomework() {
        return homework;
    }

    public void setHomework(List<HomeworkDTO> homework) {
        this.homework = homework;
    }
}

