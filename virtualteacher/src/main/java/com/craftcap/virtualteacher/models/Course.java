package com.craftcap.virtualteacher.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private long id;
    @Column(name = "picture")
    private String picture;
    @Size(min = 5, max = 255, message = "Course name should be between 5 and 255 characters.")
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "topic_id")
    @JsonIgnore
    private Topic topic;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User creator;
    @Column(name = "avg_rating")
    private double avgRating;
    @Column(name = "is_deleted")
    private boolean deleted;
    @ManyToMany(mappedBy = "enrolledCourses")
    @JsonIgnore
    private List<User> students;
    @OneToMany(mappedBy = "course")
    private List<Lecture> lectures;
    @Column(name = "is_finished")
    private boolean finished;

    public Course() {
    }

    public Course(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<User> getStudents() {
        return students;
    }

    public void setStudents(List<User> students) {
        this.students = students;
    }

    public List<Lecture> getLectures() {
        return lectures;
    }

    public void setLectures(List<Lecture> lectures) {
        this.lectures = lectures;
    }

    @Override
    public boolean equals(Object course) {
        if (!(course instanceof Course))
            return false;
        Course course1 = (Course) course;
        return this.id == course1.getId();
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }
}
