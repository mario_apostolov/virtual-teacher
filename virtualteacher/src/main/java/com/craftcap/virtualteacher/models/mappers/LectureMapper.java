package com.craftcap.virtualteacher.models.mappers;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.CourseLiteDTO;
import com.craftcap.virtualteacher.models.dtos.LectureDTO;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LectureMapper {
    private HomeworkService homeworkService;

    @Autowired
    public LectureMapper(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    public LectureDTO fromLectureToDTO(Lecture lecture) {
        LectureDTO dto = new LectureDTO();
        dto.setId(lecture.getId());
        dto.setTitle(lecture.getTitle());
        dto.setDescription(lecture.getDescription());
        return dto;
    }

    public List<LectureDTO> fromLecturesToDTOS(List<Lecture> lectures) {
        List<LectureDTO> dtos = new ArrayList<>();
        for (Lecture lecture : lectures) {
            dtos.add(fromLectureToDTO(lecture));
        }
        return dtos;
    }

    public List<LectureDTO> fromLecturesToDTOS(List<Lecture> lectures, User user) {
        List<LectureDTO> dtos = new ArrayList<>();
        for (Lecture lecture : lectures) {
            LectureDTO lectureDTO = fromLectureToDTO(lecture);
            lectureDTO.setGrade(homeworkService.getGradeByLectureAndStudent(lecture, user));
            dtos.add(lectureDTO);
        }
        return dtos;
    }
}
