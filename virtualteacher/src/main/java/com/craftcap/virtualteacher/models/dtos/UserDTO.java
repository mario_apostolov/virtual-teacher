package com.craftcap.virtualteacher.models.dtos;

import javax.validation.constraints.Size;
import java.util.List;

public class UserDTO {
    @Size(min = 1, max = 50, message = "First name should be between 1 and 50 characters.")
    private String firstName;
    @Size(min = 1, max = 50, message = "Last name should be between 1 and 50 characters.")
    private String lastName;
    private List<CourseLiteDTO> enrolledCourses;

    public UserDTO() {
    }

    public List<CourseLiteDTO> getEnrolledCourses() {
        return enrolledCourses;
    }

    public void setEnrolledCourses(List<CourseLiteDTO> enrolledCourses) {
        this.enrolledCourses = enrolledCourses;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
