package com.craftcap.virtualteacher.models.dtos;

import java.util.List;

public class TopicDTO {
    private String topic;
    private List<CourseLiteDTO> courses;

    public TopicDTO() {
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<CourseLiteDTO> getCourses() {
        return courses;
    }

    public void setCourses(List<CourseLiteDTO> courses) {
        this.courses = courses;
    }
}
