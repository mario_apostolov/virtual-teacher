package com.craftcap.virtualteacher.models;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "lectures")
public class Lecture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lecture_id")
    private long id;
    @Size(min = 3, max = 255, message = "Lecture title should be between 3 and 255 characters")
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;
    @Column(name = "video")
    private String video;
    @Column(name = "is_deleted")
    private boolean isDeleted;
    @OneToMany(mappedBy = "lecture")
    private List<Homework> homeworkOfStudents;
    @Column(name = "assignment")
    private String assignment;
    @ManyToMany(mappedBy = "finishedLectures")
    private List<User> students;

    public Lecture() {
    }

    public Lecture(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public List<Homework> getHomeworkOfStudents() {
        return homeworkOfStudents;
    }

    public void setHomeworkOfStudents(List<Homework> homeworkOfStudents) {
        this.homeworkOfStudents = homeworkOfStudents;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public List<User> getStudents() {
        return students;
    }

    public void setStudents(List<User> students) {
        this.students = students;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Lecture)){
            return false;
        }
        if (this.id == ((Lecture) obj).id){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return title.hashCode() + (int) id;
    }
}
