package com.craftcap.virtualteacher.models.mappers;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.dtos.CourseLiteDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class CourseLiteMapper {

    public CourseLiteDTO fromCourseToDTO(Course course){
        CourseLiteDTO dto = new CourseLiteDTO();
        dto.setFinished(course.isFinished());
        dto.setId(course.getId());
        dto.setTitle(course.getTitle());
        dto.setDescription(course.getDescription());
        dto.setRating(course.getAvgRating());
        dto.setPicture(course.getPicture());
        dto.setCreatorUsername(course.getCreator().getUsername());
        dto.setCreatorPicture(course.getCreator().getPicture());
        dto.setCreator(String.format("%s %s",course.getCreator().getFirstName(), course.getCreator().getLastName()));
        return dto;
    }

    public List<CourseLiteDTO> fromCoursesToDTOS(List<Course> courses){
        List<CourseLiteDTO> dtos = new ArrayList<>();
        for (Course course: courses) {
            dtos.add(fromCourseToDTO(course));
        }
        return dtos;
    }
}
