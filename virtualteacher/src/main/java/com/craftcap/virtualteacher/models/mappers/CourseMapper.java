package com.craftcap.virtualteacher.models.mappers;

import com.craftcap.virtualteacher.databaselayer.LectureRepository;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.dtos.CourseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class CourseMapper {
    private LectureMapper lectureMapper;
    private LectureRepository lectureRepository;

    @Autowired
    public CourseMapper(LectureMapper lectureMapper, LectureRepository lectureRepository) {
        this.lectureMapper = lectureMapper;
        this.lectureRepository = lectureRepository;
    }

    public CourseDTO fromCourseToDTO(Course course){
        CourseDTO dto = new CourseDTO();
        dto.setId(course.getId());
        dto.setTitle(course.getTitle());
        dto.setDescription(course.getDescription());
//        dto.setTopic(course.getTopic());
        dto.setAvgRating(course.getAvgRating());
        dto.setPicture(course.getPicture());
        dto.setLectures(lectureRepository.findAllByCourseId(course.getId())
                .stream()
                .map(lecture -> lectureMapper.fromLectureToDTO(lecture))
                .collect(Collectors.toList()));
        return dto;
    }
}
