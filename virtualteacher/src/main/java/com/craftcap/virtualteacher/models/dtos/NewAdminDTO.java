package com.craftcap.virtualteacher.models.dtos;

public class NewAdminDTO {
    private String username;
    private String password;

    public NewAdminDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
