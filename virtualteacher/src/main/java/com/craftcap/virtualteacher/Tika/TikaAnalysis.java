package com.craftcap.virtualteacher.Tika;

import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.exceptions.WrongFIleFormat;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

public class TikaAnalysis {

    public static String detectDocTypeUsingDetector(InputStream stream)
            throws IOException {
        Detector detector = new DefaultDetector();
        Metadata metadata = new Metadata();

        MediaType mediaType = detector.detect(stream, metadata);
        return mediaType.toString();
    }


    public static void checkAssignmentType(String assignmentType) {
        if (assignmentType.equals("text/javascript")
                || assignmentType.equals("text/plain")
                || assignmentType.equals("application/vnd.ms-excel")
                || assignmentType.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                || assignmentType.equals("application/vnd.ms-word.document.macroEnabled.12")
                || assignmentType.equals("application/vnd.ms-word.template.macroEnabled.12")
                || assignmentType.equals("application/msword")
                || assignmentType.equals("application/x-tika-ooxml")
                || assignmentType.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                || assignmentType.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.template")
                || assignmentType.equals("application/pdf")) {
        } else {
            throw new WrongFIleFormat(AppConstants.WRONG_FILE_FORMAT);
        }
    }

    public static void checkVideoType(String videoType) {
        if (videoType.equals("video/mp4") || videoType.equals("video/JPEG") || videoType.equals("video/quicktime")) {
        } else {
            throw new WrongFIleFormat(AppConstants.WRONG_FILE_FORMAT);
        }
    }

    public static void checkImageType(String pictureFile) {
        if (pictureFile.equals("image/png")
                || pictureFile.equals("image/gif")
                || pictureFile.equals("image/jpg")
                || pictureFile.equals("image/jpeg")) {
        } else {
            throw new WrongFIleFormat(AppConstants.WRONG_FILE_FORMAT);
        }
    }

    public static void isFileCorrectSize(MultipartFile file, long size) {
        if (file.getSize() > size) {
            throw new WrongFIleFormat(String.format("File is too large, max size is %d MB",size/1000000));
        }
    }
}