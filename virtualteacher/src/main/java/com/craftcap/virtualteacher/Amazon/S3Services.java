package com.craftcap.virtualteacher.Amazon;

import com.amazonaws.services.s3.model.S3Object;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface S3Services {
    S3Object downloadFile(String keyName);
    void uploadFile(String keyName, MultipartFile file) throws IOException;
    void uploadDefaultImage(String username) throws IOException;
}