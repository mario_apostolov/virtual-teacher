package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.Amazon.S3Services;
import com.craftcap.virtualteacher.Tika.TikaAnalysis;
import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.databaselayer.LectureRepository;
import com.craftcap.virtualteacher.exceptions.ForbiddenActionException;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.exceptions.WrongFIleFormat;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewLectureDTO;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.LectureService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LectureServiceImpl implements LectureService {
    private LectureRepository lectureRepository;
    private CourseService courseService;
    private UserService userService;
    private S3Services s3Services;

    @Autowired
    public LectureServiceImpl(LectureRepository lectureRepository, CourseService courseService, UserService userService, S3Services s3Services) {
        this.lectureRepository = lectureRepository;
        this.courseService = courseService;
        this.userService = userService;
        this.s3Services = s3Services;
    }

    @Override
    public List<Lecture> getAllByCourse(long id) {
        return lectureRepository.findAllByCourseId(id);
    }

    @Override
    public List<Lecture> getUnfinishedCourseLecturesById(long id) {
        return lectureRepository.findAllByCourseId(id);
    }

    @Override
    public Lecture getLectureById(long id) {
        Lecture lecture = lectureRepository.findLectureById(id);
        if (lecture == null)
            throw new ItemNotFoundException(AppConstants.LECTURE_NOT_FOUND_ERROR_MESSAGE);
        return lecture;
    }

    @Override
    public Lecture createLecture(NewLectureDTO newLectureDTO, long courseId) {
        Lecture lecture = new Lecture();

//        check if exist
        if(newLectureDTO.getAssignmentFile() == null || newLectureDTO.getVideoFile() == null){
            throw new WrongFIleFormat("Please upload video and assignment files!");
        } else {
            TikaAnalysis.isFileCorrectSize(newLectureDTO.getAssignmentFile(), AppConstants.ASSIGNMENT_MAX_SIZE);
            TikaAnalysis.isFileCorrectSize(newLectureDTO.getVideoFile(), AppConstants.VIDEO_MAX_SIZE);
            String assignmentType = "";
            String videoType = "";
            String assignmentFileName = String.format("%d_%s", System.currentTimeMillis(),newLectureDTO.getAssignmentFile().getOriginalFilename());
            try {
                InputStream inputStream = new BufferedInputStream(newLectureDTO.getAssignmentFile().getInputStream());
                assignmentType = TikaAnalysis.detectDocTypeUsingDetector(inputStream);
                TikaAnalysis.checkAssignmentType(assignmentType);
                s3Services.uploadFile(assignmentFileName, newLectureDTO.getAssignmentFile());
            } catch (IOException e) {
                e.printStackTrace();
                throw new WrongFIleFormat(e.getMessage());
            }
            String videoFileName = String.format("%d_%s", System.currentTimeMillis(),newLectureDTO.getVideoFile().getOriginalFilename());
            try {
                InputStream inputStream = new BufferedInputStream(newLectureDTO.getVideoFile().getInputStream());
                videoType = TikaAnalysis.detectDocTypeUsingDetector(inputStream);
                TikaAnalysis.checkVideoType(videoType);
                s3Services.uploadFile(videoFileName, newLectureDTO.getVideoFile());
            } catch (IOException e) {
                throw new WrongFIleFormat(e.getMessage());
            }
            lecture.setVideo(videoFileName);
            lecture.setAssignment(assignmentFileName);
        }
        lecture.setCourse(courseService.getUnfinishedById(courseId));
        lecture.setTitle(newLectureDTO.getTitle());
        lecture.setDescription(newLectureDTO.getDescription());
        lecture.setDeleted(false);
        return lectureRepository.save(lecture);
    }

    @Override
    public Lecture deleteLecture(long courseId, long id) {
        List<Long> lecturesIds = lectureRepository.findAllByCourseId(courseId)
                .stream()
                .map(Lecture::getId)
                .collect(Collectors.toList());
        Lecture lecture = lectureRepository.findLectureById(id);
        if (lecture == null)
            throw new ItemNotFoundException(AppConstants.LECTURE_NOT_FOUND_ERROR_MESSAGE);
        if (lecture.isDeleted())
            throw new ItemNotFoundException(AppConstants.LECTURE_ALREADY_DELETED_ERROR_MESSAGE);
        if (!lecturesIds.contains(lecture.getId()))
            throw new ForbiddenActionException(AppConstants.LECTURE_FROM_ANOTHER_COURSE_ERROR_MESSAGE);
        lecture.setDeleted(true);
        lectureRepository.save(lecture);
        return lecture;
    }

    @Override
    public List<Lecture> getFinishedLecturesByCourseByUser(long courseId, String username) {
        Course course = courseService.getUnfinishedById(courseId);
        return lectureRepository.getFinishedLecturesByCourseByUser(course, username);
    }

    @Override
    public List<Lecture> getFinishedLecturesByHomeworkByUser(long courseId, String username) {
        User user = userService.findUserByUsername(username);
        Course course = courseService.getUnfinishedById(courseId);
        return lectureRepository.getFinished(user, course);
    }

    @Override
    public Lecture getCurrentLectureByHomework(long courseId, String username) {
        List<Lecture> lecturesOfThisCourse = getAllByCourse(courseId);
        List<Lecture> finishedLectures = getFinishedLecturesByHomeworkByUser(courseId, username);
        if (lecturesOfThisCourse.size() == finishedLectures.size()) {
            return null;
        }
        return lecturesOfThisCourse.get(finishedLectures.size());
    }

    @Override
    public List<Lecture> getUnfinishedLecturesByHomeworkByUser(long courseId, String username) {
        List<Lecture> lecturesOfThisCourse = getAllByCourse(courseId);
        List<Lecture> finishedLectures = getFinishedLecturesByHomeworkByUser(courseId, username);
        if (lecturesOfThisCourse.size() == finishedLectures.size()) {
            return new ArrayList<>();
        }
        return lecturesOfThisCourse.subList(finishedLectures.size() + 1, lecturesOfThisCourse.size());
    }

    @Override
    public void isLectureIsDisplayable(String username, long lectureId, long courseId) {
        Lecture lecture = lectureRepository.findLectureById(lectureId);
        Course course = courseService.getUnfinishedById(courseId);
        if (!course.getCreator().getUsername().equals(username)) {
            if (getUnfinishedLecturesByHomeworkByUser(courseId, username).contains(lecture)) {
                throw new ForbiddenActionException(AppConstants.ACCESS_DENIED_ERROR_MESSAGE);
            }
        }
    }
}
