package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Topic;

import java.util.List;

public interface TopicService {
    List<Topic> getAll();

    List<Course> getAllCoursesByTopic(String name);

    Topic createTopic(String topic);
}
