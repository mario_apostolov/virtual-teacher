package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.databaselayer.CourseRepository;
import com.craftcap.virtualteacher.databaselayer.TopicRepository;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.services.contracts.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TopicServiceImpl implements TopicService {
    private TopicRepository topicRepository;
    private CourseRepository courseRepository;

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository, CourseRepository courseRepository) {
        this.topicRepository = topicRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Topic> getAll() {
        return topicRepository.findAll();
    }

    @Override
    public List<Course> getAllCoursesByTopic(String name){
        List<Topic> topics = topicRepository.findAllByTopic(name);
        List<Course> courses = new ArrayList<>();
        topics.stream()
                .map(topic -> courses.addAll(courseRepository.getAllByTopicAndFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc(topic)))
                .collect(Collectors.toList());
        return courses;
    }

    @Override
    public Topic createTopic(String topic){
        Topic topicToCheck = topicRepository.getTopicByTopic(topic);
        if (topicToCheck != null) {
            return topicToCheck;
        }
        else {
            Topic topicToCreate = new Topic();
            topicToCreate.setTopic(topic);
            return topicRepository.save(topicToCreate);
        }
    }
}
