package com.craftcap.virtualteacher.services.implementation;

import com.amazonaws.services.s3.AmazonS3Client;
import com.craftcap.virtualteacher.Amazon.S3Services;
import com.craftcap.virtualteacher.Tika.TikaAnalysis;
import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.databaselayer.*;
import com.craftcap.virtualteacher.exceptions.ForbiddenActionException;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.exceptions.WrongFIleFormat;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewCourseDTO;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import com.craftcap.virtualteacher.services.contracts.RatingService;
import com.craftcap.virtualteacher.services.contracts.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {
    private S3Services s3Services;
    private CourseRepository courseRepository;
    private LectureRepository lectureRepository;
    private HomeworkService homeworkService;
    private UserRepository userRepository;
    private RatingService ratingService;
    private TopicService topicService;

    @Autowired
    public CourseServiceImpl(S3Services s3Services, CourseRepository courseRepository, LectureRepository lectureRepository, HomeworkService homeworkService, UserRepository userRepository, RatingService ratingService, TopicService topicService) {
        this.s3Services = s3Services;
        this.courseRepository = courseRepository;
        this.lectureRepository = lectureRepository;
        this.homeworkService = homeworkService;
        this.userRepository = userRepository;
        this.ratingService = ratingService;
        this.topicService = topicService;
    }

    @Override
    public Page<Course> getAllCourses(Integer page, String sortField) {
        if (page == null)
            page = 1;
        if (sortField == null)
            sortField = "id";
        Pageable pageable = PageRequest.of(page - 1, AppConstants.COURSES_PER_PAGE, Sort.by(sortField).descending());
        if (sortField.equals("title"))
            pageable = PageRequest.of(page - 1, AppConstants.COURSES_PER_PAGE, Sort.by(sortField));
        return courseRepository.getAllByFinishedIsTrueAndDeletedIsFalse(pageable);
    }

    @Override
    public Course getById(long id) {
        Course course = courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(id);
        if (course == null)
            throw new ItemNotFoundException(AppConstants.COURSE_NOT_FOUND_ERROR_MESSAGE);
        return course;
    }

    @Override
    public Course getUnfinishedById(long courseId) {
        Course course = courseRepository.findCourseByIdAndDeletedIsFalse(courseId);
        if (course == null)
            throw new ItemNotFoundException(AppConstants.COURSE_NOT_FOUND_ERROR_MESSAGE);
        return course;
    }

    @Override
    public List<Course> getAllCourses() {
        return courseRepository.findCoursesByFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc();
    }

    @Override
    public List<Course> getTopThreeCoursesByRating() {
        return courseRepository.findCoursesByFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc().stream()
                .limit(3).collect(Collectors.toList());
    }

    @Override
    public Course createCourse(User teacher, NewCourseDTO newCourseDTO) {
        if (courseRepository.existsCourseByTitleAndFinishedIsTrueAndDeletedIsFalse(newCourseDTO.getTitle()))
            throw new ForbiddenActionException(AppConstants.COURSE_ALREADY_EXIST_ERROR_MESSAGE);
        Course course = new Course();
        if (newCourseDTO.getFile() == null || newCourseDTO.getFile().isEmpty()) {
            course.setPicture("course_default_photo.jpg");
        } else {
            String pictureFile = "";
            TikaAnalysis.isFileCorrectSize(newCourseDTO.getFile(), AppConstants.IMAGE_MAX_SIZE);
            String photoFileName = String.format("%d_%s", System.currentTimeMillis(), newCourseDTO.getFile().getOriginalFilename());
            try {
                InputStream inputStream = new BufferedInputStream(newCourseDTO.getFile().getInputStream());
                pictureFile = TikaAnalysis.detectDocTypeUsingDetector(inputStream);
                TikaAnalysis.checkImageType(pictureFile);
                s3Services.uploadFile(photoFileName, newCourseDTO.getFile());
            } catch (IOException e) {
                e.printStackTrace();
            }
            course.setPicture(photoFileName);
        }
        course.setTitle(newCourseDTO.getTitle());
        course.setFinished(false);
        course.setCreator(teacher);
        course.setDeleted(false);
        course.setDescription(newCourseDTO.getDescription());
        course.setTopic(topicService.createTopic(newCourseDTO.getTopic()));
        return courseRepository.save(course);
    }

    @Override
    public Course updateCourse(long courseId, String description) {
        Course course = getById(courseId);
        course.setDescription(description);
        return courseRepository.save(course);
    }

    @Override
    public Course deleteCourse(long id) {
        Course course = courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(id);
        if (course == null)
            throw new ItemNotFoundException(AppConstants.COURSE_NOT_FOUND_ERROR_MESSAGE);
        if (course.isDeleted())
            throw new ItemNotFoundException(AppConstants.COURSE_ALREADY_DELETED_ERROR_MESSAGE);
        course.setDeleted(true);
        courseRepository.save(course);
        return course;
    }

    @Override
    public List<Course> getCoursesByTeacherId(long id) {
        return courseRepository.findCoursesByCreatorIdAndFinishedIsTrueAndDeletedIsFalse(id);
    }

    @Override
    public boolean existByStudents(User user, long courseId) {
        return courseRepository.existsCourseByStudentsAndIdAndFinishedIsTrueAndDeletedIsFalse(user, courseId);
    }

    @Override
    public List<Course> getEnrolledCoursesByStudent(String username) {
        User student = findUserByUsername(username);
        return student.getEnrolledCourses()
                .stream()
                .filter(course -> !isStudentPassCourse(student.getId(), course.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Course> getCompletedCoursesByStudent(String username) {
        User student = findUserByUsername(username);
        return student.getEnrolledCourses()
                .stream()
                .filter(course -> isStudentPassCourse(student.getId(), course.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Course> getCreatedCourses(String username) {
        User user = findUserByUsername(username);
        return courseRepository.findAllByCreatorAndDeletedIsFalse(user);
    }

    @Override
    public Course setCourseToFinished(long courseId) {
        Course course = getUnfinishedById(courseId);
        course.setFinished(true);
        return courseRepository.save(course);
    }

    @Override
    public int getCourseProgress(long courseId, String username) {
        Course course = getById(courseId);
        List<Lecture> finishedLectures = lectureRepository.getFinishedLecturesByCourseByUser(course, username);
        Integer numberOfLecturesInCourse = lectureRepository.getNumberOfLecturesPerCourse(courseId);
        double progress = (Double.valueOf(finishedLectures.size()) / Double.valueOf(numberOfLecturesInCourse)) * 100;
        return (int) progress;
    }

    @Override
    public Course rateCourse(long courseId, String username, int rating) {
        Course course = getById(courseId);
        ;
        User user = findUserByUsername(username);
        if (ratingService.existsByCourseAndUser(course, user))
            throw new ForbiddenActionException(AppConstants.RATE_COURSE_TWICE_ERROR_MESSAGE);
        if (isStudentPassCourse(user.getId(), courseId)) {
            ratingService.saveRating(rating, course, user);
            calculateAvgRating(courseId, rating);
            return getById(courseId);
        } else {
            throw new ForbiddenActionException(AppConstants.RATE_COURSE_BEFORE_PASS_IT_ERROR_MESSAGE);
        }
    }

    @Override
    public boolean isStudentPassCourse(long userId, long courseId) {
        boolean isStudentPassCourse = false;
        if (lectureRepository.getNumberOfLecturesPerCourse(courseId) ==
                homeworkService.getNumberOfHomeworkPerStudentPerCourse(userId, courseId))
            if (homeworkService.getAvgGradeByCourseByStudent(userId, courseId) > AppConstants.AVG_GRADE_TO_PASS) {
                isStudentPassCourse = true;
            }
        return isStudentPassCourse;
    }

    @Override
    public List<Integer> getPages(Page<Course> courses) {
        List<Integer> pages = new ArrayList<>();
        for (int i = 1; i < courses.getTotalPages() + 1; i++) {
            pages.add(i);
        }
        return pages;
    }


    private void calculateAvgRating(long courseId, int rating) {
        Course course = getById(courseId);
        if (course.getAvgRating() == 0)
            course.setAvgRating(rating);
        else
            course.setAvgRating((course.getAvgRating() + rating) / 2);
        courseRepository.save(course);
    }

    @Override
    public List<Course> getAllByTopicAndFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc(Topic topic) {
        return courseRepository.getAllByTopicAndFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc(topic);
    }

    @Override
    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }

    private User findUserByUsername(String username) {
        User user = userRepository.findUserByUsernameAndEnabledIsTrue(username);
        if (user == null)
            throw new ItemNotFoundException(AppConstants.USER_NOT_FOUND_ERROR_MESSAGE);
        return user;
    }
}
