package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.databaselayer.SearchRepository;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.services.contracts.SearchService;
import com.craftcap.virtualteacher.services.contracts.TopicService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SearchServiceImpl implements SearchService {
    private SearchRepository searchRepository;
    private UserService userService;
    private TopicService topicService;

    @Autowired
    public SearchServiceImpl(SearchRepository searchRepository, UserService userService, TopicService topicService) {
        this.searchRepository = searchRepository;
        this.userService = userService;
        this.topicService = topicService;
    }

    @Override
    public Page<Course> searchCourses(String something, Integer page, String sort) {
        if (page == null)
            page = 1;
        if (sort == null)
            sort = "id";
        Set<Course> courses = new HashSet<>();
        courses.addAll(searchRepository.searchCourseByTitle(something));
        courses.addAll(topicService.getAllCoursesByTopic(something));
        courses.addAll(searchCoursesByUserFirstOrLastName(something));
        if (courses.size() == 0)
            courses.addAll(searchRepository.searchCourseByDescriptionAndFinishedIsTrueAndDeletedIsFalse(something));
        Pageable pageable = PageRequest.of(page - 1, AppConstants.COURSES_PER_PAGE, Sort.by(sort).descending());
        List<Course> coursesToReturn = new ArrayList<>(courses);
        return new PageImpl<>(coursesToReturn, pageable, coursesToReturn.size());
    }

    @Override
    public Page<Course> filteredCourses(Integer page, String sort, String title, String topic, String teacher) {
        if (page == null)
            page = 1;
        if (sort == null)
            sort = "id";
        Pageable pageable = PageRequest.of(page - 1, AppConstants.COURSES_PER_PAGE, Sort.by(sort).descending());
        List<Course> coursesByTitle = new ArrayList<>();
        List<Course> coursesByTopic = new ArrayList<>();
        List<Course> coursesByCreator = new ArrayList<>();
        if (!title.isEmpty()) {
            coursesByTitle = searchRepository.searchCourseByTitle(title);
        }
        if (!topic.isEmpty()) {
            if (coursesByTitle.size() != 0) {
                coursesByTopic = coursesByTitle.stream()
                        .filter(course -> course.getTopic().getTopic().equals(topic))
                        .collect(Collectors.toList());
            } else
                coursesByTopic = topicService.getAllCoursesByTopic(topic);
        }
        List<User> teachers = new ArrayList<>();
        if (!teacher.isEmpty())
            teachers = userService.searchUserByFirstNameOrLastName(teacher);
        if (coursesByTopic.size() != 0) {
            for (User user : teachers) {
                coursesByCreator.addAll(coursesByTopic.stream()
                        .filter(course -> course.getCreator().getUsername().equals(user.getUsername()))
                        .collect(Collectors.toList()));

            }
        } else if (coursesByTitle.size() != 0) {
            for (User user : teachers) {
                coursesByCreator.addAll(coursesByTitle.stream()
                        .filter(course -> course.getCreator().getUsername().equals(user.getUsername()))
                        .collect(Collectors.toList()));
            }
        } else {
            for (User user : teachers) {
                coursesByCreator.addAll(searchRepository.findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(user));
            }
        }
        if (coursesByCreator.size() == 0) {
            if (coursesByTopic.size() == 0) {
                coursesByCreator.addAll(coursesByTitle);
            }
            coursesByCreator.addAll(coursesByTopic);
        }
        return new PageImpl<>(coursesByCreator, pageable, coursesByCreator.size());
    }

    @Override
    public List<Course> searchCoursesByUserFirstOrLastName(String name) {
        List<User> authorByFirstName = userService.searchUserByFirstNameOrLastName(name);
        List<Course> courses = new ArrayList<>();
        authorByFirstName.stream()
                .map(user -> courses.addAll(searchRepository.findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(user)))
                .collect(Collectors.toList());
        return courses;
    }
}
