package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.databaselayer.UserRepository;
import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.services.contracts.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminServiceImpl implements AdminService {
    private UserRepository userRepository;

    @Autowired
    public AdminServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllEnabledStudents() {
        return userRepository.findAllByEnabledIsTrueAndRole(Role.STUDENT);
    }

    @Override
    public List<User> getAllEnabledTeachers() {
        return userRepository.findAllByEnabledIsTrueAndRole(Role.TEACHER);
    }

    @Override
    public List<User> getAllEnabledWaiting() {
        return userRepository.findAllByEnabledIsTrueAndRole(Role.WAITING);
    }
  @Override
    public List<User> getAllEnabledAdmin() {
        return userRepository.findAllByEnabledIsTrueAndRole(Role.ADMIN);
    }



}
