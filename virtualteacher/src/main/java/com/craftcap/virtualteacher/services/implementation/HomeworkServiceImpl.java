package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.Amazon.S3Services;
import com.craftcap.virtualteacher.Tika.TikaAnalysis;
import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.databaselayer.CourseRepository;
import com.craftcap.virtualteacher.databaselayer.HomeworkRepository;
import com.craftcap.virtualteacher.databaselayer.LectureRepository;
import com.craftcap.virtualteacher.databaselayer.UserRepository;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.exceptions.WrongFIleFormat;
import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewHomeworkDTO;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class HomeworkServiceImpl implements HomeworkService {
    private HomeworkRepository homeworkRepository;
    private UserRepository userRepository;
    private CourseRepository courseRepository;
    private LectureRepository lectureRepository;
    private S3Services s3Services;

    @Autowired
    public HomeworkServiceImpl(HomeworkRepository homeworkRepository, UserRepository userRepository, CourseRepository courseRepository, LectureRepository lectureRepository, S3Services s3Services) {
        this.homeworkRepository = homeworkRepository;
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
        this.lectureRepository = lectureRepository;
        this.s3Services = s3Services;
    }

    @Override
    public List<Homework> getAllByCourseId(long id) {
        int grade = 0;
        return homeworkRepository.findAllByCourseIdAndGrade(id, grade);
    }

    @Override
    public Homework evaluateHomework(Homework homework) {
        return homeworkRepository.save(homework);
    }

    @Override
    public Homework getById(long id) {
        Homework homework = homeworkRepository.findHomeworkById(id);
        if (homework == null)
            throw new ItemNotFoundException(AppConstants.HOMEWORK_NOT_FOUND_ERROR_MESSAGE);
        return homework;
    }

    @Override
    public Homework createHomework(String username, long courseId, long lectureId, NewHomeworkDTO newHomeworkDTO) {
        Homework homeworkToSave = new Homework();
        String homeworkFileName = String.format("%d_%s", System.currentTimeMillis(), newHomeworkDTO.getHomeworkFile().getOriginalFilename());
        String homeworkType = "";
        TikaAnalysis.isFileCorrectSize(newHomeworkDTO.getHomeworkFile(), AppConstants.HOMEWORK_MAX_SIZE);
        try {
            InputStream inputStream = new BufferedInputStream(newHomeworkDTO.getHomeworkFile().getInputStream());
            homeworkType = TikaAnalysis.detectDocTypeUsingDetector(inputStream);
            TikaAnalysis.checkAssignmentType(homeworkType);
            s3Services.uploadFile(homeworkFileName, newHomeworkDTO.getHomeworkFile());
        } catch (IOException e) {
            throw new WrongFIleFormat(e.getMessage());
        }
        homeworkToSave.setHomework(homeworkFileName);
        homeworkToSave.setLecture(lectureRepository.findLectureById(lectureId));
        homeworkToSave.setCourse(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(courseId));
        homeworkToSave.setStudent(userRepository.findUserByUsernameAndEnabledIsTrue(username));
        homeworkToSave.setGrade(0);
        homeworkToSave.setHomework(newHomeworkDTO.getHomeworkFile().getOriginalFilename());
        return homeworkRepository.save(homeworkToSave);
    }

    @Override
    public int getAvgGradeByCourseByStudent(long userId, long courseId) {
        Integer avgGrade = homeworkRepository.getAvgGradeByCourseByStudent(userId, courseId);
        if (avgGrade == null) {
            avgGrade = 0;
        }
        return avgGrade;
    }

    @Override
    public int getNumberOfHomeworkPerStudentPerCourse(long userId, long courseId) {
        return homeworkRepository.getNumberOfHomeworkPerStudentPerCourse(userId, courseId);
    }

    @Override
    public int getGradeByLectureAndStudent(Lecture lecture, User student) {
        Homework homework = homeworkRepository.findHomeworkByLectureAndStudent(lecture, student);
        if (homework == null)
            return 0;
        return homework.getGrade();
    }
}
