package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewUserDTO;
import com.craftcap.virtualteacher.models.dtos.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();

    Page<User> getAllTeachers(Integer pageable, Role role);

    User getByUsername(String username);

    User create(NewUserDTO newUserDTO);

    User enrollToCourse(String name, long courseId);

    User deleteUser(String username);

    User getUserByRoleAndUsername(Role role, String username);

    User updateUser(User user);

    User findUserByUsername(String username);

    List<User> searchUserByFirstNameOrLastName(String name);

    void isUserIsCreator(String username, long courseId);
}
