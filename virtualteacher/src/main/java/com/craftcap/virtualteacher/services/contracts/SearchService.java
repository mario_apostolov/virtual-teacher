package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.Course;
import org.springframework.data.domain.Page;

import java.util.List;

public interface SearchService {
    Page<Course> searchCourses(String something, Integer page, String sort);

    Page<Course> filteredCourses(Integer page, String sort, String title, String topic, String teacher);

    List<Course> searchCoursesByUserFirstOrLastName(String name);
}
