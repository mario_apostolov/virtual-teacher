package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.Amazon.S3Services;
import com.craftcap.virtualteacher.Tika.TikaAnalysis;
import com.craftcap.virtualteacher.constants.AppConstants;
import com.craftcap.virtualteacher.databaselayer.UserRepository;
import com.craftcap.virtualteacher.exceptions.ForbiddenActionException;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.exceptions.WrongFIleFormat;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewUserDTO;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private CourseService courseService;
    private PasswordEncoder passwordEncoder;
    private UserDetailsManager userDetailsManager;
    private S3Services s3Services;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, CourseService courseService, PasswordEncoder passwordEncoder, UserDetailsManager userDetailsManager, S3Services s3Services) {
        this.userRepository = userRepository;
        this.courseService = courseService;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;
        this.s3Services = s3Services;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> getAllTeachers(Integer page, Role role) {
        if (page == null)
            page = 1;
        Pageable pageable = PageRequest.of(page - 1, AppConstants.COURSES_PER_PAGE);
        return userRepository.findAllByRole(pageable, role);
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.findUserByUsernameAndEnabledIsTrue(username);
        if (user == null)
            throw new ItemNotFoundException(AppConstants.USER_NOT_FOUND_ERROR_MESSAGE);
        return user;
    }

    @Override
    public User create(NewUserDTO newUserDTO) {
        String username = newUserDTO.getUsername().trim();
        if(username.length() < 4){
            throw new ForbiddenActionException(AppConstants.USERNAME_LENGTH_ERROR_MESSAGE);
        }
        newUserDTO.setUsername(username);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        newUserDTO.getUsername(),
                        passwordEncoder.encode(newUserDTO.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);

        User userToCreate = getByUsername(newUserDTO.getUsername());
        if (newUserDTO.getFile().isEmpty() || newUserDTO.getFile() == null) {

        } else {

            String pictureFile = "";
            String photoFileName = newUserDTO.getUsername();
            TikaAnalysis.isFileCorrectSize(newUserDTO.getFile(), AppConstants.IMAGE_MAX_SIZE);
            try {
                InputStream inputStream = new BufferedInputStream(newUserDTO.getFile().getInputStream());
                pictureFile = TikaAnalysis.detectDocTypeUsingDetector(inputStream);
                TikaAnalysis.checkImageType(pictureFile);
                s3Services.uploadFile(photoFileName, newUserDTO.getFile());
            } catch (IOException e) {
                throw new WrongFIleFormat(e.getMessage());
            }
            userToCreate.setPicture(photoFileName);
        }
        if (newUserDTO.getRole().equals(Role.TEACHER)) userToCreate.setRole(Role.WAITING);
        if (newUserDTO.getRole().equals(Role.STUDENT)) userToCreate.setRole(Role.STUDENT);
        User user = getByUsername(newUserDTO.getUsername());
        userToCreate.setId(user.getId());
        userToCreate.setEmail(newUserDTO.getEmail());
        userToCreate.setFirstName(newUserDTO.getFirstName());
        userToCreate.setLastName(newUserDTO.getLastName());
        return userRepository.save(userToCreate);
    }

    @Override
    public User enrollToCourse(String username, long courseId) {
        User user = userRepository.findUserByUsernameAndEnabledIsTrue(username);
        if (user == null)
            throw new ItemNotFoundException(AppConstants.USER_NOT_FOUND_ERROR_MESSAGE);
        Course course = courseService.getById(courseId);
        if (course == null)
            throw new ItemNotFoundException(AppConstants.COURSE_NOT_FOUND_ERROR_MESSAGE);
        List<Course> enrolledCourses = user.getEnrolledCourses();
        if (enrolledCourses.contains(course))
            throw new ForbiddenActionException(AppConstants.ENROLL_TO_COURSE_TWICE_ERROR_MESSAGE);
        enrolledCourses.add(course);
        user.setEnrolledCourses(enrolledCourses);
        return userRepository.save(user);
    }

    @Override
    public User deleteUser(String username) {
        User user = userRepository.findUserByUsernameAndEnabledIsTrue(username);
        if (user == null)
            throw new ItemNotFoundException(AppConstants.USER_NOT_FOUND_ERROR_MESSAGE);
        if (!user.isEnabled())
            throw new ItemNotFoundException(AppConstants.USER_ALREADY_DELETED_ERROR_MESSAGE);
        if (user.getRole() == Role.TEACHER) {
            List<Course> createdCourses = courseService.getCreatedCourses(username);
            for (Course course : createdCourses) {
                course.setDeleted(true);
                courseService.saveCourse(course);
            }
        }
        user.setEnabled(false);
        userRepository.save(user);
        return user;
    }

    @Override
    public User getUserByRoleAndUsername(Role role, String username) {
        if (userRepository.findUserByRoleAndUsername(role, username) == null)
            throw new ItemNotFoundException(AppConstants.USER_NOT_FOUND_ERROR_MESSAGE);
        return userRepository.findUserByRoleAndUsername(role, username);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findUserByUsername(String username) {
        User user = userRepository.findUserByUsernameAndEnabledIsTrue(username);
        if (user == null) {
            throw new ItemNotFoundException(AppConstants.USER_NOT_FOUND_ERROR_MESSAGE);
        }
        return user;
    }

    @Override
    public List<User> searchUserByFirstNameOrLastName(String name) {
        return userRepository.searchUserByFirstNameOrLastName(name);
    }

    @Override
    public void isUserIsCreator(String username, long courseId) {
        Course course = courseService.getUnfinishedById(courseId);
        if (!course.getCreator().getUsername().equals(username)) {
            throw new ForbiddenActionException(AppConstants.ACCESS_DENIED_ERROR_MESSAGE);
        }
    }
}
