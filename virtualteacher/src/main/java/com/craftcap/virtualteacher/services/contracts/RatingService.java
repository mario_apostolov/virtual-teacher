package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Rating;
import com.craftcap.virtualteacher.models.User;

public interface RatingService {
    boolean isCourseRatedByUser(Course course, User user);

    boolean existsByCourseAndUser(Course course, User user);

    Rating saveRating(int rating, Course course, User user);
}
