package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.databaselayer.CourseRepository;
import com.craftcap.virtualteacher.databaselayer.UserRepository;
import com.craftcap.virtualteacher.services.contracts.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageServiceImpl implements ImageService {
    private UserRepository userRepository;
    private CourseRepository courseRepository;

    @Autowired
    public ImageServiceImpl(CourseRepository courseRepository, UserRepository userRepository) {
        this.userRepository = userRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public String getUserPictureByName(String username) {
        return userRepository.findUserByUsernameAndEnabledIsTrue(username).getPicture();
    }

    @Override
    public String getCoursePictureById(long courseId) {
        return courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(courseId).getPicture();
    }
}
