package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.User;

import java.util.List;

public interface AdminService {
    List<User> getAllEnabledStudents();
    List<User> getAllEnabledWaiting();
    List<User> getAllEnabledTeachers();

    List<User> getAllEnabledAdmin();
}
