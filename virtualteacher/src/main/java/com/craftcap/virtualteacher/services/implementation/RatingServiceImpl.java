package com.craftcap.virtualteacher.services.implementation;

import com.craftcap.virtualteacher.databaselayer.RatingRepository;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Rating;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {
    private RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public boolean isCourseRatedByUser(Course course, User user) {
        return ratingRepository.existsByCourseAndUser(course,user);
    }

    @Override
    public boolean existsByCourseAndUser(Course course, User user) {
        return ratingRepository.existsByCourseAndUser(course, user);
    }

    @Override
    public Rating saveRating(int rating, Course course, User user) {
        Rating ratingToSave = new Rating();
        ratingToSave.setRating(rating);
        ratingToSave.setCourse(course);
        ratingToSave.setUser(user);
        return ratingRepository.save(ratingToSave);
    }
}
