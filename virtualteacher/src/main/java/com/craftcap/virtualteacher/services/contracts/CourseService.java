package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewCourseDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CourseService {

    Page<Course> getAllCourses(Integer page, String sortField);

    Course getById(long id);
    Course getUnfinishedById(long courseId);

    List<Course> getAllCourses();

    List<Course> getTopThreeCoursesByRating();

    Course createCourse(User teacher, NewCourseDTO newCourseDTO);

    Course updateCourse(long courseId, String description);

    Course deleteCourse(long id);

    List<Course> getCoursesByTeacherId(long id);

    boolean existByStudents(User user, long id);

    boolean isStudentPassCourse(long courseId, long userId);

    List<Course> getEnrolledCoursesByStudent(String username);

    List<Course> getCompletedCoursesByStudent(String username);

    List<Course> getCreatedCourses(String username);

    Course setCourseToFinished(long courseId);

    int getCourseProgress(long courseId, String username);

    Course rateCourse(long courseId, String username, int rating);

    List<Integer> getPages(Page<Course> corses);

    List<Course> getAllByTopicAndFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc(Topic topic);

    Course saveCourse(Course course);
}
