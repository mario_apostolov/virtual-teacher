package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.dtos.NewLectureDTO;

import java.util.List;

public interface LectureService {
    List<Lecture> getAllByCourse(long id);
    List<Lecture> getUnfinishedCourseLecturesById(long id);
    Lecture getLectureById(long id);
    Lecture createLecture(NewLectureDTO newLectureDTO, long courseId);
    Lecture deleteLecture(long courseId,long id);

    List<Lecture> getFinishedLecturesByCourseByUser(long courseId, String username);

    List<Lecture> getFinishedLecturesByHomeworkByUser(long courseId, String username);

    Lecture getCurrentLectureByHomework(long courseId, String username);

    List<Lecture> getUnfinishedLecturesByHomeworkByUser(long courseId, String username);

    void isLectureIsDisplayable(String username, long lectureId, long courseId);
}
