package com.craftcap.virtualteacher.services.contracts;

import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.models.dtos.NewHomeworkDTO;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.User;

import java.util.List;

public interface HomeworkService {
    List<Homework> getAllByCourseId(long id);

    Homework evaluateHomework(Homework homework);

    Homework getById(long id);

    Homework createHomework(String username ,long courseId, long lectureId, NewHomeworkDTO newHomeworkDTO);

    int getAvgGradeByCourseByStudent(long userId, long courseId);

    int getNumberOfHomeworkPerStudentPerCourse(long userId, long courseId);

    int getGradeByLectureAndStudent(Lecture lecture, User student);
}
