package com.craftcap.virtualteacher.services.contracts;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {
    String getUserPictureByName(String username);
    String getCoursePictureById(long courseId);
}
