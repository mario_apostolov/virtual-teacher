package com.craftcap.virtualteacher;

import com.craftcap.virtualteacher.databaselayer.CourseRepository;
import com.craftcap.virtualteacher.databaselayer.TopicRepository;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.services.implementation.TopicServiceImpl;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class TopicServiceTests {
    @Mock
    private TopicRepository topicRepository;
    @Mock
    private CourseRepository courseRepository;

    @InjectMocks
    private TopicServiceImpl topicService;

    @Test
    public void getAll_Should_ReturnTopic_When_TopicExist (){
        List<Topic> topics = new ArrayList<>();
        topics.add(new Topic(2,"Name2"));
        Mockito.when(topicRepository.findAll())
                .thenReturn(topics);
        // Act

        List<Topic> result = topicService.getAll();

        // Assert
        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAll_Should_ReturnEmptyList_When_TopicNotExist(){
        List<Topic> topics = new ArrayList<>();
        Mockito.when(topicRepository.findAll())
                .thenReturn(topics);
        // Act
        List<Topic> result = topicService.getAll();

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getAllCoursesByTopic_Should_Return_Courses_If_Topic_Exist(){
        Topic topic = new Topic(2,"Name2");
        List<Topic> topics = new ArrayList<>();
        topics.add(topic);
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1, "Course1"));
        Mockito.when(topicRepository.findAllByTopic("Name2"))
                .thenReturn(topics);
        Mockito.when(courseRepository.getAllByTopicAndFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc(topic))
                .thenReturn(courses);

        List<Course> result = topicService.getAllCoursesByTopic("Name2");

        Assert.assertEquals(1, result.get(0).getId());
    }

    @Test
    public void getAllCoursesByTopic_Should_ReturnEmptyList_When_TopicNotExist(){

        List<Course> result = topicService.getAllCoursesByTopic("Name2");

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void createTopic_Should_Create_Topic_If_Not_Exist(){
        Mockito.when(topicRepository.getTopicByTopic("Name2"))
                .thenReturn(null);
        Topic topic = new Topic(2,"Name2");
        Mockito.when(topicRepository.save(any()))
                .thenReturn(topic);

        Topic result = topicService.createTopic("Name2");

        Assert.assertEquals("Name2", result.getTopic());
    }

    @Test
    public void createTopic_Should_Return_Topic_If__Exist(){
        Topic topic = new Topic(2,"Name2");
        Mockito.when(topicRepository.getTopicByTopic("Name2"))
                .thenReturn(topic);

        Topic result = topicService.createTopic("Name2");

        Assert.assertEquals(2, result.getId());
    }
}
