package com.craftcap.virtualteacher;

import com.craftcap.virtualteacher.databaselayer.UserRepository;
import com.craftcap.virtualteacher.models.Role;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.services.implementation.AdminServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceTests {
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private AdminServiceImpl adminService;

    @Test
    public void getAllEnabledStudents_Should_ReturnUser_When_UserExist(){
        List<User> users = new ArrayList<>();
        users.add(new User(2, "Name2"));
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.STUDENT))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledStudents();

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAllEnabledStudents_Should_ReturnEmptyList_When_UserNotExist(){
        List<User> users = new ArrayList<>();
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.STUDENT))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledStudents();

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getAllEnabledTeachers_Should_ReturnUser_When_UserExist(){
        List<User> users = new ArrayList<>();
        users.add(new User(2, "Name2"));
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.TEACHER))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledTeachers();

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAllEnabledTeachers_Should_ReturnEmptyList_When_UserNotExist(){
        List<User> users = new ArrayList<>();
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.TEACHER))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledTeachers();

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getAllEnabledWaiting_Should_ReturnUser_When_UserExist(){
        List<User> users = new ArrayList<>();
        users.add(new User(2, "Name2"));
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.WAITING))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledWaiting();

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAllEnabledWaiting_Should_ReturnEmptyList_When_UserNotExist(){
        List<User> users = new ArrayList<>();
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.WAITING))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledWaiting();

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getAllEnabledAdmin_Should_ReturnUser_When_UserExist(){
        List<User> users = new ArrayList<>();
        users.add(new User(2, "Name2"));
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.ADMIN))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledAdmin();

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAllEnabledAdmin_Should_ReturnEmptyList_When_UserNotExist(){
        List<User> users = new ArrayList<>();
        Mockito.when(userRepository.findAllByEnabledIsTrueAndRole(Role.ADMIN))
                .thenReturn(users);

        List<User> result = adminService.getAllEnabledAdmin();

        Assert.assertEquals(0, result.size());
    }
}
