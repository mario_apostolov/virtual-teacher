package com.craftcap.virtualteacher;

import com.craftcap.virtualteacher.databaselayer.UserRepository;
import com.craftcap.virtualteacher.exceptions.ForbiddenActionException;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.services.implementation.UserServiceImpl;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {
    @Mock
    private UserRepository userRepository;
    @Mock
    private CourseService courseService;

    @InjectMocks
    private UserServiceImpl service;

    @Test
    public void getAll_Should_ReturnUser_When_UserExist (){
        List<User> users = new ArrayList<>();
        users.add(new User(2,"Name2"));
        Mockito.when(userRepository.findAll())
                .thenReturn(users);
        // Act

        List<User> result = service.getAllUsers();

        // Assert
        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAll_Should_ReturnEmptyList_When_UserNotExist(){
        List<User> users = new ArrayList<>();
        Mockito.when(userRepository.findAll())
                .thenReturn(users);
        // Act
        List<User> result = service.getAllUsers();

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getById_Should_ReturnUser_When_UserExist (){
        User user = new User(2,"Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        // Act

        User result = service.getByUsername("Name2");

        // Assert
        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void getById_Should_Throw_When_UserNotExist(){
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(null);
        // Act
        User result = service.getByUsername("Name2");
    }

    @Test
    public void enrollToCourse_Should_ReturnUser_When_UserAndCourseExistAndUserIsNotEnrollToItYet(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        Course course = new Course(1, "Course1");
        Mockito.when(courseService.getById(1L))
                .thenReturn(course);
        Mockito.when(userRepository.save(any()))
                .thenReturn(user);

        User result = service.enrollToCourse("Name2", 1L);

        Assert.assertEquals(1, result.getEnrolledCourses().get(0).getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void enrollToCourse_Should_ThrowItemNotFoundException_When_UserNotExist(){

        User result = service.enrollToCourse("Name2", 1L);
    }

    @Test(expected = ItemNotFoundException.class)
    public void enrollToCourse_Should_ThrowItemNotFoundException_When_CourseNotExist(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);

        User result = service.enrollToCourse("Name2", 1L);
    }

    @Test(expected = ForbiddenActionException.class)
    public void enrollToCourse_Should_ThrowForbiddenActionException_When_UserIsAlreadyEnrolledToThisCourse(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        Course course = new Course(1, "Course1");
        Mockito.when(courseService.getById(1L))
                .thenReturn(course);
        List<Course> enrolledCourses = user.getEnrolledCourses();
        enrolledCourses.add(course);
        user.setEnrolledCourses(enrolledCourses);

        User result = service.enrollToCourse("Name2", 1L);
    }

    @Test
    public void deleteUser_Should_ReturnUser_When_UserExistAndIsNotDeletedYet(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        user.setEnabled(true);
        Mockito.when(userRepository.save(any()))
                .thenReturn(user);

        User result = service.deleteUser("Name2");

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void deleteUser_Should_ThrowItemNotFoundException_When_UserNotExist(){

        User result = service.deleteUser("Name2");
    }

    @Test(expected = ItemNotFoundException.class)
    public void deleteUser_Should_ThrowItemNotFoundException_When_UserIsAlreadyDeleted(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);

        User result = service.deleteUser("Name2");
    }
}
