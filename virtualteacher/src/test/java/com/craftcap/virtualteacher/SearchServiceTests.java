package com.craftcap.virtualteacher;

import com.craftcap.virtualteacher.databaselayer.SearchRepository;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Topic;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.services.implementation.SearchServiceImpl;
import com.craftcap.virtualteacher.services.contracts.TopicService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceTests {
    @Mock
    private SearchRepository searchRepository;
    @Mock
    private UserService userService;
    @Mock
    private TopicService topicService;

    @InjectMocks
    private SearchServiceImpl searchService;


    @Test
    public void searchCoursesByUserFirstOrLastName_Should_ReturnCorse_IfCoursesAndUserExist(){
        User user = new User(2, "Name2");
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.searchUserByFirstNameOrLastName("Name2"))
                .thenReturn(users);
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1, "course1"));
        Mockito.when(searchRepository.findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(user))
                .thenReturn(courses);

        List<Course> result = searchService.searchCoursesByUserFirstOrLastName("Name2");

        Assert.assertEquals(1, result.get(0).getId());
    }

    @Test
    public void searchCoursesByUserFirstOrLastName_Should_ReturnCourse_IfCoursesAndUserExistByLast_Name(){
        User user = new User(2, "Name2");
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.searchUserByFirstNameOrLastName("Name2"))
                .thenReturn(users);
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1, "course1"));
        Mockito.when(searchRepository.findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(user))
                .thenReturn(courses);

        List<Course> result = searchService.searchCoursesByUserFirstOrLastName("Name2");

        Assert.assertEquals(1, result.get(0).getId());
    }

    @Test
    public void searchCoursesByUserFirstOrLastName_Should_ReturnEmptyList_IfCoursesNotExists(){
        User user = new User(2, "Name2");
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.searchUserByFirstNameOrLastName("Name2"))
                .thenReturn(users);
        List<Course> courses = new ArrayList<>();
        Mockito.when(searchRepository.findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(user))
                .thenReturn(courses);

        List<Course> result = searchService.searchCoursesByUserFirstOrLastName("Name2");

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void searchCoursesByUserFirstOrLastName_Should_ReturnEmptyList_IfUserNotExist(){
        List<User> users = new ArrayList<>();
        Mockito.when(userService.searchUserByFirstNameOrLastName("Name2"))
                .thenReturn(users);

        List<Course> result = searchService.searchCoursesByUserFirstOrLastName("Name2");

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void searchCourses_Should_ReturnCourses_When_CourseExist(){
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(2, "Name2"));
        Mockito.when(searchRepository.searchCourseByTitle("Name2"))
                .thenReturn(courses);
        Mockito.when(topicService.getAllCoursesByTopic("Name2"))
                .thenReturn(courses);

        Page<Course> result = searchService.searchCourses("Name2", null, null);

        Assert.assertEquals(2, result.getContent().get(0).getId());
    }

    @Test
    public void searchCourses_Should_ReturnEmptyPage_When_CourseNotExist(){

        Page<Course> result = searchService.searchCourses("Name2", 1, "id");

        Assert.assertEquals(0, result.getTotalElements());
    }

    @Test
    public void filteredCourses_Should_ReturnCourses_When_CourseByTitleExist(){
        Course course = new Course(2, "Name2");
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        Mockito.when(searchRepository.searchCourseByTitle("Name2"))
                .thenReturn(courses);

        Page<Course> result = searchService.filteredCourses(null, null, "Name2", "", "");

        Assert.assertEquals(2, result.getContent().get(0).getId());
    }

    @Test
    public void filteredCourses_Should_ReturnCourse_When_CoursesByTitleExistWithSameTopic(){
        Course course = new Course(2, "Name2");
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        Topic topic = new Topic(1, "topic");
        course.setTopic(topic);
        Mockito.when(searchRepository.searchCourseByTitle("Name2"))
                .thenReturn(courses);

        Page<Course> result = searchService.filteredCourses(null, null, "Name2", "topic", "");

        Assert.assertEquals(1, result.getTotalElements());
    }

    @Test
    public void filteredCourses_Should_ReturnCourses_When_CourseByTopicExist(){
        Course course = new Course(2, "Name2");
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        Topic topic = new Topic(1, "topic");
        course.setTopic(topic);
        Mockito.when(topicService.getAllCoursesByTopic("topic"))
                .thenReturn(courses);

        Page<Course> result = searchService.filteredCourses(null, null, "", "topic", "");

        Assert.assertEquals(2, result.getContent().get(0).getId());
    }

    @Test
    public void filteredCourses_Should_ReturnCourse_When_CoursesByTopicExistWithSameTeacher(){
        Course course = new Course(2, "Name2");
        User user = new User(1,"User");
        user.setUsername("username");
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        Topic topic = new Topic(1, "topic");
        course.setTopic(topic);
        course.setCreator(user);
        Mockito.when(topicService.getAllCoursesByTopic("topic"))
                .thenReturn(courses);
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.searchUserByFirstNameOrLastName("User"))
                .thenReturn(users);

        Page<Course> result = searchService.filteredCourses(null, null, "Name2", "topic", "User");

        Assert.assertEquals(1, result.getTotalElements());
    }

    @Test
    public void filteredCourses_Should_ReturnCourse_When_CoursesByTitleExistWithSameTeacher(){
        Course course = new Course(2, "Name2");
        User user = new User(1,"User");
        user.setUsername("username");
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        course.setCreator(user);
        Mockito.when(searchRepository.searchCourseByTitle("Name2"))
                .thenReturn(courses);
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.searchUserByFirstNameOrLastName("User"))
                .thenReturn(users);

        Page<Course> result = searchService.filteredCourses(null, null, "Name2", "", "User");

        Assert.assertEquals(1, result.getTotalElements());
    }

    @Test
    public void filteredCourses_Should_ReturnCourse_When_CoursesByTeacherExist(){
        Course course = new Course(2, "Name2");
        User user = new User(1,"User");
        user.setUsername("username");
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        course.setCreator(user);
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.searchUserByFirstNameOrLastName("User"))
                .thenReturn(users);
        Mockito.when(searchRepository.findAllByCreatorAndFinishedIsTrueAndDeletedIsFalse(any()))
                .thenReturn(courses);

        Page<Course> result = searchService.filteredCourses(null, null, "", "", "User");

        Assert.assertEquals(1, result.getTotalElements());
    }

    @Test
    public void filteredCourses_Should_ReturnEmptyList_When_CoursesByTitleOrTopicOrTeacherNotExist(){
        User user = new User(1,"User");
        user.setUsername("username");
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userService.searchUserByFirstNameOrLastName("User"))
                .thenReturn(users);

        Page<Course> result = searchService.filteredCourses(null, null, "", "", "User");

        Assert.assertEquals(0, result.getTotalElements());
    }

}
