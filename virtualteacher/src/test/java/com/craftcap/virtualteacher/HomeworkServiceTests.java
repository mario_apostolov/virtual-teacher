package com.craftcap.virtualteacher;

import com.craftcap.virtualteacher.databaselayer.HomeworkRepository;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.models.Homework;
import com.craftcap.virtualteacher.services.implementation.HomeworkServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class HomeworkServiceTests {
    @Mock
    private HomeworkRepository homeworkRepository;

    @InjectMocks
    private HomeworkServiceImpl homeworkService;

    @Test
    public void getAllByCourseId_Should_ReturnHomework_WhenHomeworkExist(){
        List<Homework> homework = new ArrayList<>();
        homework.add(new Homework(2, "Name2"));
        Mockito.when(homeworkRepository.findAllByCourseIdAndGrade(1L, 0))
                .thenReturn(homework);

        List<Homework> result = homeworkService.getAllByCourseId(1L);

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAllByCourseId_Should_ReturnEmptyList_WhenHomeworkNotExist(){

        List<Homework> result = homeworkService.getAllByCourseId(1L);

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getById_Should_ReturnHomework_WhenHomeworkExist(){
        Homework homework = new Homework(2, "Name2");
        Mockito.when(homeworkRepository.findHomeworkById(2L))
                .thenReturn(homework);

        Homework result = homeworkService.getById(2L);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void getById_Should_ThrowItemNotFoundException_WhenHomeworkNotExist(){

        Homework result = homeworkService.getById(2L);
    }

    @Test
    public void getAvgGradeByCourseByStudent_Should_ReturnInt_When_IsCalled(){

        int result = homeworkService.getAvgGradeByCourseByStudent(1L, 2L);

        Assert.assertEquals(0, result);
    }

    @Test
    public void getNumberOfHomeworkPerStudentPerCourse_Should_ReturnInt_When_CourseAndUserExist(){
        Mockito.when(homeworkRepository.getNumberOfHomeworkPerStudentPerCourse(1L, 2L))
                .thenReturn(3);

        int result = homeworkService.getNumberOfHomeworkPerStudentPerCourse(1L, 2L);

        Assert.assertEquals(3, result);
    }
}
