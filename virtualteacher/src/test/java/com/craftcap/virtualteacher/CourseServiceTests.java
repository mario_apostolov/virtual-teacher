package com.craftcap.virtualteacher;

import com.craftcap.virtualteacher.databaselayer.*;
import com.craftcap.virtualteacher.exceptions.ForbiddenActionException;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.models.*;
import com.craftcap.virtualteacher.models.dtos.NewCourseDTO;
import com.craftcap.virtualteacher.services.contracts.HomeworkService;
import com.craftcap.virtualteacher.services.contracts.RatingService;
import com.craftcap.virtualteacher.services.contracts.TopicService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import com.craftcap.virtualteacher.services.implementation.CourseServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class CourseServiceTests {
    @Mock
    private CourseRepository courseRepository;
    @Mock
    private LectureRepository lectureRepository;
    @Mock
    private HomeworkService homeworkService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private RatingService ratingService;
    @Mock
    private TopicService topicService;

    @InjectMocks
    private CourseServiceImpl courseService;

    @Test
    public void getAllCourses_Should_Return_IfCourseExist(){
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(2, "Name2"));
        Page<Course> coursePage = new PageImpl<>(courses);
        Mockito.when(courseRepository.getAllByFinishedIsTrueAndDeletedIsFalse(any()))
                .thenReturn(coursePage);

        Page<Course> result = courseService.getAllCourses(1, "title");

        Assert.assertEquals(1, result.getTotalPages());
    }

    @Test
    public void getAllCourses_Should_ReturnEmptyList_IfCourseNotExist() {
        List<Course> courses = new ArrayList<>();
        Page<Course> coursePage = new PageImpl<>(courses);
        Mockito.when(courseRepository.getAllByFinishedIsTrueAndDeletedIsFalse(any()))
                .thenReturn(coursePage);

        Page<Course> result = courseService.getAllCourses(null, null);

        Assert.assertEquals(0, result.getTotalElements());
    }

    @Test
    public void getById_Should_Return_When_CourseExist(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(course);

        Course result = courseService.getById(2L);

        Assert.assertEquals("Name2", result.getTitle());
    }

    @Test(expected = ItemNotFoundException.class)
    public void getById_Should_ThrowItemNotFoundException_When_CourseNotExist(){
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(null);

        Course result = courseService.getById(2L);
    }

    @Test
    public void getUnfinishedById_Should_ReturnCourse_When_CourseExist(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndDeletedIsFalse(2L))
                .thenReturn(course);

        Course result = courseService.getUnfinishedById(2L);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void getUnfinishedById_Should_ThrowItemNotFoundException_When_CourseNotExist(){

        Course result = courseService.getUnfinishedById(2L);
    }

    @Test
    public void getTopThreeCoursesByRating_Should_ReturnCourses_When_CoursesExist(){
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(2, "Name2"));
        Mockito.when(courseRepository.findCoursesByFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc())
                .thenReturn(courses);

        List<Course> result = courseService.getTopThreeCoursesByRating();

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getTopThreeCoursesByRating_Should_ReturnEmptyList_When_CoursesNotExist(){
        List<Course> courses = new ArrayList<>();
        Mockito.when(courseRepository.findCoursesByFinishedIsTrueAndDeletedIsFalseOrderByAvgRatingDesc())
                .thenReturn(courses);

        List<Course> result = courseService.getTopThreeCoursesByRating();

        Assert.assertEquals(0, result.size());
    }

    @Test(expected = ForbiddenActionException.class)
    public void createCourse_Should_ThrowForbiddenActionException_When_CourseExist(){
        Mockito.when(courseRepository.existsCourseByTitleAndFinishedIsTrueAndDeletedIsFalse("Name2"))
                .thenReturn(true);

        Course result = courseService.createCourse(new User(), new NewCourseDTO("Name2"));
    }

    @Test
    public void deleteCourse_Should_ReturnCourse_When_CourseExist(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.save(any()))
                .thenReturn(course);
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(course);

        Course result = courseService.deleteCourse(2L);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void deleteCourse_Should_ThrowItemNotFoundException_When_CourseNotExist(){

        Course result = courseService.deleteCourse(2L);
    }

    @Test
    public void getCoursesByTeacherId_Should_ReturnCourses_When_CoursesExist(){
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(2, "Name2"));
        Mockito.when(courseRepository.findCoursesByCreatorIdAndFinishedIsTrueAndDeletedIsFalse(1L))
                .thenReturn(courses);

        List<Course> result = courseService.getCoursesByTeacherId(1L);

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getCoursesByTeacherId_ReturnEmptyList_When_CoursesNotExist(){
        List<Course> courses = new ArrayList<>();
        Mockito.when(courseRepository.findCoursesByCreatorIdAndFinishedIsTrueAndDeletedIsFalse(1L))
                .thenReturn(courses);

        List<Course> result = courseService.getCoursesByTeacherId(1L);

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void isStudentPassCourse_Should_ReturnTrue_IfStudentPass(){
        Mockito.when(homeworkService.getAvgGradeByCourseByStudent(1L, 2L))
                .thenReturn(100);
        Mockito.when(lectureRepository.getNumberOfLecturesPerCourse(2L))
                .thenReturn(10);
        Mockito.when(homeworkService.getNumberOfHomeworkPerStudentPerCourse(1L, 2L))
                .thenReturn(10);

        Assert.assertTrue(courseService.isStudentPassCourse(1L, 2L));
    }

    @Test
    public void isStudentPassCourse_Should_ReturnFalse_IfStudentDoNotHaveAvgGradeForThisCourse(){
        Assert.assertFalse(courseService.isStudentPassCourse(1L, 2L));
    }

    @Test
    public void isStudentPassCourse_Should_ReturnFalse_IfStudentNotAllHomeworkPresent(){
        Mockito.when(lectureRepository.getNumberOfLecturesPerCourse(2L))
                .thenReturn(10);
        Mockito.when(homeworkService.getNumberOfHomeworkPerStudentPerCourse(1L, 2L))
                .thenReturn(9);

        Assert.assertFalse(courseService.isStudentPassCourse(1L, 2L));
    }

    @Test
    public void isStudentPassCourse_Should_ReturnFalse_IfStudentHasLowAvgGrade(){
        Mockito.when(homeworkService.getAvgGradeByCourseByStudent(1L, 2L))
                .thenReturn(10);
        Mockito.when(lectureRepository.getNumberOfLecturesPerCourse(2L))
                .thenReturn(10);
        Mockito.when(homeworkService.getNumberOfHomeworkPerStudentPerCourse(1L, 2L))
                .thenReturn(10);

        Assert.assertFalse(courseService.isStudentPassCourse(1L, 2L));
    }

    @Test
    public void getEnrolledCoursesByStudent_Should_ReturnCourse_When_UserAndCoursesExist(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1, "Course1"));
        user.setEnrolledCourses(courses);

        List<Course> result = courseService.getEnrolledCoursesByStudent("Name2");

        Assert.assertEquals(1, result.get(0).getId());
    }

    @Test
    public void getEnrolledCoursesByStudent_Should_ReturnEmptyList_When_CourseNotExist(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        List<Course> courses = new ArrayList<>();
        user.setEnrolledCourses(courses);

        List<Course> result = courseService.getEnrolledCoursesByStudent("Name2");

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getCompletedCoursesByStudent_Should_ReturnCourse_When_UserAndCoursesExist(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1, "Course1"));
        user.setEnrolledCourses(courses);
        Mockito.when(homeworkService.getAvgGradeByCourseByStudent(2L, 1L))
                .thenReturn(100);
        Mockito.when(lectureRepository.getNumberOfLecturesPerCourse(1L))
                .thenReturn(10);
        Mockito.when(homeworkService.getNumberOfHomeworkPerStudentPerCourse(2L, 1L))
                .thenReturn(10);

        List<Course> result = courseService.getCompletedCoursesByStudent("Name2");

        Assert.assertEquals(1, result.get(0).getId());
    }

    @Test
    public void getCompletedCoursesByStudent_Should_ReturnEmptyList_When_CourseNotExist(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);
        List<Course> courses = new ArrayList<>();
        user.setEnrolledCourses(courses);

        List<Course> result = courseService.getCompletedCoursesByStudent("Name2");

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getCreatedCourses_Should_ReturnCourse_When_UserAndCoursesExist(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("username"))
                .thenReturn(user);
        List<Course> courses = new ArrayList<>();
        courses.add(new Course(1, "Course1"));
        Mockito.when(courseRepository.findAllByCreatorAndDeletedIsFalse(any()))
                .thenReturn(courses);

        List<Course> result = courseService.getCreatedCourses("username");

        Assert.assertEquals(1, result.get(0).getId());
    }

    @Test
    public void getCreatedCourses_Should_ReturnEmptyList_When_CourseNotExist(){
        User user = new User(2, "Name2");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("Name2"))
                .thenReturn(user);

        List<Course> result = courseService.getCreatedCourses("Name2");

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void setCourseToFinished_Should_ReturnCourse_When_CourseIsExist(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndDeletedIsFalse(2L))
                .thenReturn(course);
        Mockito.when(courseRepository.save(any()))
                .thenReturn(course);

        Course result = courseService.setCourseToFinished(2L);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void setCourseToFinished_Should_ThrowItemNotFoundException_When_CourseIsNotExist(){

        Course result = courseService.setCourseToFinished(2L);
    }

    @Test
    public void getCourseProgress_Should_ReturnInt_When_CourseExist(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(course);
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture(1, "Lecture1"));
        lectures.add(new Lecture(2, "Lecture2"));
        Mockito.when(lectureRepository.getFinishedLecturesByCourseByUser(course, "username"))
                .thenReturn(lectures);
        Mockito.when(lectureRepository.getNumberOfLecturesPerCourse(2L))
                .thenReturn(4);

        int result = courseService.getCourseProgress(2L, "username");

        Assert.assertEquals(50, result);
    }

    @Test
    public void rateCourse_Should_ReturnCourse_When_CourseAndUserExistUserPassCourseAndUserIsNotRatedThisCourseYet(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(course);
        User user = new User(1, "User1");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("User1"))
                .thenReturn(user);
        Mockito.when(ratingService.existsByCourseAndUser(any(), any()))
                .thenReturn(false);
        Mockito.when(homeworkService.getAvgGradeByCourseByStudent(1L, 2L))
                .thenReturn(100);
        Mockito.when(lectureRepository.getNumberOfLecturesPerCourse(2L))
                .thenReturn(10);
        Mockito.when(homeworkService.getNumberOfHomeworkPerStudentPerCourse(1L, 2L))
                .thenReturn(10);
        Rating rating = new Rating();
        Mockito.when(ratingService.saveRating(anyInt(), any(), any()))
                .thenReturn(rating);
        Mockito.when(courseRepository.save(any()))
                .thenReturn(course);

        Course result = courseService.rateCourse(2L, "User1", 5);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void rateCourse_Should_ThrowItemNotFoundException_When_CourseNotExist(){

        Course result = courseService.rateCourse(2L, "User1", 5);
    }

    @Test(expected = ForbiddenActionException.class)
    public void rateCourse_Should_ThrowForbiddenActionException_When_UserTryToRateCourseTwice(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(course);
        User user = new User(1, "User1");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("User1"))
                .thenReturn(user);
        Mockito.when(ratingService.existsByCourseAndUser(any(), any()))
                .thenReturn(true);

        Course result = courseService.rateCourse(2L, "User1", 5);
    }

    @Test(expected = ForbiddenActionException.class)
    public void rateCourse_Should_ThrowForbiddenActionException_When_UserTryToRateCourseBeforePassIt(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(course);
        User user = new User(1, "User1");
        Mockito.when(userRepository.findUserByUsernameAndEnabledIsTrue("User1"))
                .thenReturn(user);
        Mockito.when(ratingService.existsByCourseAndUser(any(), any()))
                .thenReturn(false);

        Course result = courseService.rateCourse(2L, "User1", 5);
    }

    @Test
    public void updateCourse_Should_ReturnCourse_When_CourseExist(){
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.findCourseByIdAndFinishedIsTrueAndDeletedIsFalse(2L))
                .thenReturn(course);
        Mockito.when(courseRepository.save(any()))
                .thenReturn(course);

        Course result = courseService.updateCourse(2L, "description");

        Assert.assertEquals(2, result.getId());
    }

    @Test
    public void getPages_Should_ReturnInt_When_PageExist(){
        Course course = new Course(2, "Name2");
        List<Course> courses = new ArrayList<>();
        courses.add(course);
        Page<Course> coursePage = new PageImpl<>(courses);

        List<Integer> result = courseService.getPages(coursePage);

        Assert.assertEquals(Integer.valueOf(1), result.get(0));
    }

    @Test
    public void createCourse_Should_CreateNewCourse_When_CourseNotExist(){
        NewCourseDTO newCourseDTO = new NewCourseDTO();
        Course course = new Course(2, "Name2");
        Mockito.when(courseRepository.save(any()))
                .thenReturn(course);
        Mockito.when(topicService.createTopic(null))
                .thenReturn(new Topic(1,"Topic"));

        Course result = courseService.createCourse(new User(), newCourseDTO);

        Assert.assertEquals(2, result.getId());
    }
}
