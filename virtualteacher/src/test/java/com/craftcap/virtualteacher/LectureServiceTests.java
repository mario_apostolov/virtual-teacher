package com.craftcap.virtualteacher;

import com.craftcap.virtualteacher.databaselayer.LectureRepository;
import com.craftcap.virtualteacher.exceptions.ForbiddenActionException;
import com.craftcap.virtualteacher.exceptions.ItemNotFoundException;
import com.craftcap.virtualteacher.models.Course;
import com.craftcap.virtualteacher.models.Lecture;
import com.craftcap.virtualteacher.models.User;
import com.craftcap.virtualteacher.models.dtos.NewLectureDTO;
import com.craftcap.virtualteacher.services.implementation.LectureServiceImpl;
import com.craftcap.virtualteacher.services.contracts.CourseService;
import com.craftcap.virtualteacher.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;

@RunWith(MockitoJUnitRunner.class)
public class LectureServiceTests {
    @Mock
    private LectureRepository lectureRepository;
    @Mock
    private MockMultipartFile mockAssignment = new MockMultipartFile("data", "../../Files/Assignments/test_assignment.txt".getBytes());
    @Mock
    private MockMultipartFile mockVideo = new MockMultipartFile("data", "../../Files/Videos/test_video.mp4".getBytes());
    @Mock
    private CourseService courseService;
    @Mock
    private UserService userService;

    @InjectMocks
    private LectureServiceImpl lectureService;

    @Test
    public void getAllByCourse_Should_ReturnLectures_When_LecturesExist(){
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture(2, "Name2"));
        Mockito.when(lectureRepository.findAllByCourseId(1L))
                .thenReturn(lectures);

        List<Lecture> result = lectureService.getAllByCourse(1L);

        Assert.assertEquals(2, result.get(0).getId());
    }

    @Test
    public void getAllByCourse_Should_ReturnEmptyList_When_LecturesNotExist(){

        List<Lecture> result = lectureService.getAllByCourse(1L);

        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getLectureById_Should_ReturnLecture_When_LectureExist(){
        Lecture lecture = new Lecture(2, "Name2");
        Mockito.when(lectureRepository.findLectureById(2L))
                .thenReturn(lecture);

        Lecture result = lectureService.getLectureById(2L);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void getLectureById_Should_ThrowItemNotFoundException_When_LectureNotExist(){

        Lecture result = lectureService.getLectureById(2L);
    }

    @Test
    public void deleteLecture_Should_ReturnLecture_When_LectureExistAndIsFromThisCourse(){
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(new Lecture(2, "Name2"));
        Mockito.when(lectureRepository.findAllByCourseId(1L))
                .thenReturn(lectures);
        Lecture lecture = new Lecture(2, "Name2");
        Mockito.when(lectureRepository.findLectureById(2L))
                .thenReturn(lecture);
        Mockito.when(lectureRepository.save(any()))
                .thenReturn(lecture);

        Lecture result = lectureService.deleteLecture(1L, 2L);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = ItemNotFoundException.class)
    public void deleteLecture_Should_ThrowItemNotFoundException_When_LectureNotExist(){

        Lecture result = lectureService.deleteLecture(1L, 2L);
    }

    @Test(expected = ItemNotFoundException.class)
    public void deleteLecture_Should_ThrowItemNotFoundException_When_LectureIsAlreadyDeleted(){
        Lecture lecture = new Lecture(2, "Name2");
        Mockito.when(lectureRepository.findLectureById(2L))
                .thenReturn(lecture);
        lecture.setDeleted(true);

        Lecture result = lectureService.deleteLecture(1L, 2L);
    }

    @Test(expected = ForbiddenActionException.class)
    public void deleteLecture_Should_ThrowForbiddenActionException_When_LectureIsNotFromThisCourse(){
        Lecture lecture = new Lecture(2, "Name2");
        Mockito.when(lectureRepository.findLectureById(2L))
                .thenReturn(lecture);

        Lecture result = lectureService.deleteLecture(3L, 2L);
    }

    @Test
    public void getFinishedLecturesByCourseByUser_Should_ReturnLecture_WhenFinishedLectureExist(){
        Course course = new Course(2, "Name2");
        User user = new User(1, "User");
        Lecture lecture = new Lecture(3, "Lecture");
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(lecture);

        Mockito.when(lectureRepository.getFinishedLecturesByCourseByUser(any(), any()))
                .thenReturn(lectures);

        List<Lecture> result = lectureService.getFinishedLecturesByCourseByUser(2L, "username");

        Assert.assertEquals(3, result.get(0).getId());
    }

    @Test
    public void getFinishedLecturesByHomeworkByUser_Should_ReturnLecture_When_FinishedLecturesExist(){
        User user = new User(2, "Name2");
        Mockito.when(userService.findUserByUsername("username"))
                .thenReturn(user);
        Lecture lecture = new Lecture(3, "Lecture");
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(lecture);
        Mockito.when(lectureRepository.getFinished(any(), any()))
                .thenReturn(lectures);

        List<Lecture> result = lectureService.getFinishedLecturesByHomeworkByUser(2L, "username");

        Assert.assertEquals(3, result.get(0).getId());
    }

    @Test
    public void getCurrentLectureByHomework_Should_ReturnLecture_When_LectureExist(){
        User user = new User(2, "Name2");
        Mockito.when(userService.findUserByUsername("username"))
                .thenReturn(user);
        Lecture lecture = new Lecture(3, "Lecture");
        List<Lecture> lectures = new ArrayList<>();
        Mockito.when(lectureRepository.getFinished(any(), any()))
                .thenReturn(lectures);
        List<Lecture> lectureList = new ArrayList<>();
        lectureList.add(lecture);
        Mockito.when(lectureRepository.findAllByCourseId(2L))
                .thenReturn(lectureList);

        Lecture result = lectureService.getCurrentLectureByHomework(2L, "username");

        Assert.assertEquals(3, result.getId());
    }

    @Test
    public void getCurrentLectureByHomework_Should_ReturnNull_When_CourseIsFinished(){
        User user = new User(2, "Name2");
        Mockito.when(userService.findUserByUsername("username"))
                .thenReturn(user);
        Course course = new Course(1, "Course");
        Mockito.when(courseService.getUnfinishedById(1L))
                .thenReturn(course);
        Lecture lecture = new Lecture(3, "Lecture");
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(lecture);
        Mockito.when(lectureRepository.getFinished(any(), any()))
                .thenReturn(lectures);
        List<Lecture> lectureList = new ArrayList<>();
        lectureList.add(lecture);
        Mockito.when(lectureRepository.findAllByCourseId(1L))
                .thenReturn(lectureList);

        Lecture result = lectureService.getCurrentLectureByHomework(1L, "username");

        Assert.assertNull(result);
    }

    @Test
    public void getUnfinishedLecturesByHomeworkByUser_Should_ReturnLecture_When_UnfinishedLecturesExist(){
        Lecture lecture = new Lecture(3, "Lecture");
        Lecture lecture1 = new Lecture(4, "Lecture1");
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(lecture);
        lectures.add(lecture1);
        Mockito.when(lectureRepository.findAllByCourseId(1L))
                .thenReturn(lectures);
        User user = new User(2, "Name2");
        Mockito.when(userService.findUserByUsername("username"))
                .thenReturn(user);
        Course course = new Course(1, "Course");
        Mockito.when(courseService.getUnfinishedById(1L))
                .thenReturn(course);
        List<Lecture> lectureList = new ArrayList<>();
        Mockito.when(lectureRepository.getFinished(any(), any()))
                .thenReturn(lectureList);

        List<Lecture> result = lectureService.getUnfinishedLecturesByHomeworkByUser(1L, "username");

        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getUnfinishedLecturesByHomeworkByUser_Should_ReturnEmptyList_When_CourseIsFinished(){
        Lecture lecture = new Lecture(3, "Lecture");
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(lecture);
        Mockito.when(lectureRepository.findAllByCourseId(1L))
                .thenReturn(lectures);
        User user = new User(2, "Name2");
        Mockito.when(userService.findUserByUsername("username"))
                .thenReturn(user);
        Course course = new Course(1, "Course");
        Mockito.when(courseService.getUnfinishedById(1L))
                .thenReturn(course);
        Mockito.when(lectureRepository.getFinished(any(), any()))
                .thenReturn(lectures);

        List<Lecture> result = lectureService.getUnfinishedLecturesByHomeworkByUser(1L, "username");

        Assert.assertEquals(0, result.size());
    }

    @Test(expected = ForbiddenActionException.class)
    public void isLectureIsDisplayable_Should_ThrowForbiddenActionException_When_UserIsNotAuthorizedToSeeTheLecture(){
        Lecture lecture = new Lecture(2, "Name2");
        Mockito.when(lectureRepository.findLectureById(2L))
                .thenReturn(lecture);

        Course course = new Course(1, "Course");
        User user = new User(1, "User");
        user.setUsername("name");
        course.setCreator(user);
        Mockito.when(courseService.getUnfinishedById(1L))
                .thenReturn(course);

        List<Lecture> lectures = new ArrayList<>();
        Lecture lecture1 = new Lecture(4, "Lecture4");
        lectures.add(lecture1);
        lectures.add(lecture);
        Mockito.when(lectureRepository.findAllByCourseId(1L))
                .thenReturn(lectures);
        User user1 = new User(3, "User1");
        Mockito.when(userService.findUserByUsername("username"))
                .thenReturn(user1);
        Mockito.when(courseService.getUnfinishedById(1L))
                .thenReturn(course);
        List<Lecture> lectureList = new ArrayList<>();
        Mockito.when(lectureRepository.getFinished(any(), any()))
                .thenReturn(lectureList);

        lectureService.isLectureIsDisplayable("username", 2, 1);
    }

//    @Test
//    public void createLecture_Should_ReturnLecture_WhenLectureIsCreated(){
//        NewLectureDTO newLectureDTO = new NewLectureDTO();
//        newLectureDTO.setTitle("Name2");
//        newLectureDTO.setAssignmentFile(mockAssignment);
//        newLectureDTO.setVideoFile(mockVideo);
//        Course course = new Course(1, "Course");
//        Mockito.when(courseService.getUnfinishedById(1L))
//                .thenReturn(course);
//        Lecture lecture = new Lecture(3, "Lecture");
//        Mockito.when(lectureRepository.save(any()))
//                .thenReturn(lecture);
//
//        Lecture result = lectureService.createLecture(newLectureDTO, 1L);
//
//        Assert.assertEquals(3, result.getId());
//    }
}
